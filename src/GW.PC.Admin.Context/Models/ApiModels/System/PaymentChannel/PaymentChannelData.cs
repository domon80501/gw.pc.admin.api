﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentChannel
{
    public class PaymentChannelData
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("merchantName")]
        public string MerchantName { get; set; }
        [JsonProperty("providerName")]
        public string ProviderName { get; set; }
        [JsonProperty("paymentPlatformName")]
        public string PaymentPlatformName { get; set; }
        [JsonProperty("paymentTypeName")]
        public string PaymentTypeName { get; set; }
        [JsonProperty("paymentMethodName")]
        public string PaymentMethodName { get; set; }
        [JsonProperty("totalLimit")]
        public string TotalLimit { get; set; }
        [JsonProperty("minDepositAmount")]
        public string MinDepositAmount { get; set; }
        [JsonProperty("maxDepositAmount")]
        public string MaxDepositAmount { get; set; }
        [JsonProperty("lastDepostAt")]
        public string LastDepositAt { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
        // 传空值给前端做 table
        [JsonProperty("successRate")]
        public string SuccessRate { get; set; }
    }
}

﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineBanking;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Trade
{
    [TestClass]
    public class OnlineBankingTest
    {
        private OnlineBankingClient _client;
        public OnlineBankingClient OnlineBankingClient
        {
            get
            {
                if (_client == null)
                    _client = new OnlineBankingClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            OnlineBankingQuery req = new OnlineBankingQuery();

            //req.MerchantId = 1;
            //req.CustomerUserName = "";
            //req.PaymentCenterOrderNumber = "";
            //req.MerchantOrderNumber = "";
            //req.DepositStatus = Models.DepositStatus.AutoSuccess;
            //req.CreateAtStart = DateTime.Now;
            //req.CreateAtEnd = DateTime.Now;

            var result = await OnlineBankingClient.Search(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var onlineBanking = await new OnlineBankingDepositService().Add(new OnlineBankingDeposit
            {
                Username = "onlineBankingTest" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                MerchantOrderNumber = "CK1812141118571616",
                PaymentPlatform = PaymentPlatform.PC,
                RequestedAmount = 10,
                Status = DepositStatus.Unknown,
                Commission = 10,
                MerchantCallbackStatus = MerchantCallbackStatus.Await,
                CreatedAt = DateTime.Now,
                PaymentChannelId = 1,
                MerchantId = 1,
                PaymentProviderBankId = 1,
            });
            var result = await OnlineBankingClient.FindById(onlineBanking.Id);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

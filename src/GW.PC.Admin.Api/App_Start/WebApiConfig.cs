﻿using GW.PC.Services.EF;
using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Api.Handlers;
using System.Data.Entity;
using System.Web.Http;
using GW.PC.Core;
using Serilog;
using Serilog.Events;
using System;

namespace GW.PC.Admin.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.MessageHandlers.Add(new ApiResultMessageHandler());
            config.Filters.Add(new Attribute.ParameterBindingAttribute());
            config.Filters.Add(new ApiExceptionAttribute());

            Database.SetInitializer<PCContext>(null);


            #region serilog setting

            string path = AppDomain.CurrentDomain.BaseDirectory;
            SerilogLogger.Configure(() =>
                new LoggerConfiguration()
                .Enrich.WithProperty("SourceContext", "PaymentCenter Admin Api")
                .Enrich.WithProperty("Environment", "test")
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .WriteTo.File($"{path}\\logs\\serilog.txt", restrictedToMinimumLevel: LogEventLevel.Information, outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Properties:j} {Message:lj}{NewLine}{Exception}", rollingInterval: RollingInterval.Day)
                .CreateLogger()
            ); 

            #endregion
        }
    }
}

﻿using GW.PC.Admin.Context.Models.ApiModels;
using GW.PC.Admin.Context.Models.ApiModels.Trade.QQWallet;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class QQWalletClient : AdminApiClients
    {
        public QQWalletClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "qqwallet";
        }

        public async Task<ApiResult<List<QQWalletData>>> Search(QQWalletQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<QQWalletData>>(uri);
        }
        //public async Task<ApiResult<DataSourceResult<QQWalletData>>> Search(QQWalletQuery req)
        //{
        //    uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
        //    return await GetApiResultAsync<DataSourceResult<QQWalletData>>(uri);
        //}

        public async Task<ApiResult<QQWalletDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<QQWalletDTO>(uri);
        }
    }
}

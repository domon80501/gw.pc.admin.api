﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.JDWallet;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class JDWalletController : BaseController
    {
        [Route("jdwallet")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<JDWalletData>> Get([FromUri]JDWalletQuery req)
        {
            var jdWalletDeposits = await factory.JDWalletDepositService.QueryAsync(req.Content, "Merchant" , "PaymentChannel");

            var result = jdWalletDeposits.Select(o => new JDWalletData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        [Route("jdwallet")]
        public async Task<JDWalletDTO> Get(int id)
        {
            var jdWallet = await factory.JDWalletDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new JDWalletDTO()
            {
                Id = jdWallet.Id,
                MerchantName = jdWallet.Merchant.Name,
                CustomerUserName = jdWallet.Username,
                DepositIP = jdWallet.DepositIP,
                Amount = jdWallet.ActualAmount.HasValue ? jdWallet.ActualAmount : jdWallet.RequestedAmount,
                PaymentChannelName = jdWallet.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(jdWallet.Status),
                Commission = jdWallet.Commission,
                DepositAddress = jdWallet.DepositAddress,
                CreateAt = jdWallet.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = jdWallet.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = jdWallet.MerchantOrderNumber,
                SystemOrderNumber = jdWallet.SystemOrderNumber,
                PaymentCenterOrderNumber = jdWallet.PaymentCenterOrderNumber,
                PaymentOrderNumber = jdWallet.PaymentOrderNumber,
                ManualApprovedAt = jdWallet.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = jdWallet.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
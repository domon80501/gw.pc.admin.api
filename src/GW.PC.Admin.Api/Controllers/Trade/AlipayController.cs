﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.Alipay;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class AlipayController : BaseController
    {
        [Route("alipay")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<AlipayData>> Get([FromUri]AlipayQuery req)
        {
            var alipayDeposits = await factory.AlipayDepositService.QueryAsync(req.Content, "Merchant", "PaymentChannel");

            var result = alipayDeposits.Select(o => new AlipayData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });
            
            return await Task.FromResult(result);
        }

        [Route("alipay")]
        public async Task<AlipayDTO> Get(int id)
        {
            var alipay = await factory.AlipayDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new AlipayDTO()
            {
                Id = alipay.Id,
                MerchantName = alipay.Merchant.Name,
                CustomerUserName = alipay.Username,
                DepositIP = alipay.DepositIP,
                Amount = alipay.ActualAmount.HasValue ? alipay.ActualAmount : alipay.RequestedAmount,
                PaymentChannelName = alipay.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(alipay.Status),
                Commission = alipay.Commission,
                DepositAddress = alipay.DepositAddress,
                CreateAt = alipay.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = alipay.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = alipay.MerchantOrderNumber,
                SystemOrderNumber = alipay.SystemOrderNumber,
                PaymentCenterOrderNumber = alipay.PaymentCenterOrderNumber,
                PaymentOrderNumber = alipay.PaymentOrderNumber,
                ManualApprovedAt = alipay.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = alipay.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.System.User;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.System
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystem)]
    public class UserController : BaseController
    {
        [Route("user")]
        public void Options()
        { }

        [Route("user")]
        [CustomAuthorization(Permission = PermissionCode.System_User_Access)]
        public async Task<IEnumerable<UserListData>> Get([FromUri]UserQuery req)
        {
            var users = await factory.UserService.QueryAsync(req.Content, "Roles");

            var result = users.AsEnumerable().Select(o => new UserListData()
            {
                Id = o.Id,
                UserName = o.Username,
                Status = (int)o.Status,
                CreatedByUserName = o.CreatedBy?.Username,
                CreatedAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss")
            });

            return await Task.FromResult(result);
        }

        [Route("user")]
        public async Task<UserDetail> Get(int id)
        {
            var user = await factory.UserService.Find(id, "Roles");

            var result = new UserDetail()
            {
                Id = user.Id,
                UserName = user.Username,
                Status = (int)user.Status,
                CreatedByUserName = user.CreatedBy?.Username,
                CreatedAt = user.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                UserRoles = user.Roles.Select(o => o.Id).ToList()
            };

            return await Task.FromResult(result);
        }

        [Route("user")]
        [CustomAuthorization(Permission = PermissionCode.System_User_Create)]
        public async Task Post(UserCreate req)
        {
            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            User user = await
                factory.UserService.Create(new User
                {
                    Username = req.UserName,
                    CreatedAt = DateTime.Now.ToE8(),
                    CreatedById = createId,
                    Status = EntityStatus.Enabled,
                    Password = req.Password
                }, req.RoleId);
        }

        [Route("user")]
        [CustomAuthorization(Permission = PermissionCode.System_User_Detail_Edit)]
        public async Task Put(UserUpdate req)
        {
            var user = await factory.UserService.Find(req.Id, "Roles");
            await factory.UserService.Update(user, req.RoleId);
        }

        [Route("user")]
        [CustomAuthorization(Permission = PermissionCode.System_User_Detail_Status_Edit)]
        public async Task Patch(UserStatus req)
        {
            int createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? 1 : int.Parse(HttpContext.Current.User.Identity.Name);
            await factory.UserService.UpdateEntityStatus(req.Id, (EntityStatus)req.Status, createId);
        }
    }
}
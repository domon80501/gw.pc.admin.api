﻿using GW.PC.Web.Core;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade.Feature
{
    public class ManualClient : AdminApiClients
    {
        public ManualClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiTradeFeature;
            uri = "manual";
        }

        public async Task<ApiResult> ManualDeposit(int id, int paymentMethod)
        {
            uri = $"{uri}?id={id}&paymentMethod={paymentMethod}";
            return await GetApiResultAsync(uri);
        }
    }
}

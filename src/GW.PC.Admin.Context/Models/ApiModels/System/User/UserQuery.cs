﻿using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.System.User
{
    public class UserQuery
    {
        [JsonProperty("userName")]
        public string Username { get; set; }
        [JsonProperty("roleId")]
        public int? RoleId { get; set; }
        [JsonProperty("status")]
        public EntityStatus? Status { get; set; }

        public Expression<Func<PC.Models.User, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.User>(true);

                if (!string.IsNullOrEmpty(Username))
                    query = query.And(x => x.Username.Contains(Username));

                if (RoleId.HasValue)
                {
                    var roles =
                        query = query.And(x => x.Roles.Count != 0 && x.Roles.Select(o => o.Id).ToList().Contains(RoleId.Value));
                }

                if (Status.HasValue)
                    query = query.And(x => x.Status == Status);

                return query;
            }
        }
    }
}

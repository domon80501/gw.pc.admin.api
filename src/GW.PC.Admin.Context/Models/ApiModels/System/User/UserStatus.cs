﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.User
{
    public class UserStatus
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
    }
}

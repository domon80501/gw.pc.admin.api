﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.System.EntityLog;
using GW.PC.Models;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.System
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystem)]
    public class EntityLogController : BaseController
    {
        [Route("entityLog")]
        [CustomAuthorization(Permission = PermissionCode.System_EntityLog_Access)]
        public async Task<IEnumerable<EntityLogData>> Get([FromUri]EntityLogQuery req)
        {
            var entityLogs = await factory.EntityLogService.QueryAsync(req.Content);

            var result = entityLogs.Select(o => new EntityLogData()
            {
                TargetType = o.TargetType.ToString(),
                TargetId = o.TargetId.ToString(),
                Content = string.Format(
                            EntityLogContents.Get(o.Content), 
                            string.IsNullOrEmpty(o.Data) ?  new string[10] : JsonConvert.DeserializeObject<string[]>(o.Data)),
                CreatedAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss")
            });

            return await Task.FromResult(result);
        }

        [Route("entityLog")]
        public async Task<EntityLogData> Get(int id)
        {
            var entityLog = await factory.EntityLogService.Find(id);

            return new EntityLogData()
            {
                Details = entityLog.Details
            };
        }
    }


}
﻿using GW.PC.Admin.Context.Models.ApiModels;
using GW.PC.Web.Core;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace GW.PC.Admin.Api.Attribute
{
    public class ParameterBindingAttribute : ActionFilterAttribute
    {
        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var arguments = actionContext.ActionDescriptor.GetParameters();

            if (arguments.Count == 1)
            {
                var argument = arguments.Single();

                object model = actionContext.ActionArguments[argument.ParameterName];

                // all methods data in the contnt
                model = actionContext.ActionArguments[argument.ParameterName];

                //if (actionContext.Request.Method == HttpMethod.Get)
                //{
                //    if (actionContext.Request.RequestUri.Query.Length > 1)
                //    {
                //        model = WebUtility.DeserializeQueryString(
                //            Uri.UnescapeDataString(actionContext.Request.RequestUri.Query.Substring(1)),
                //            argument.ParameterType);
                //    }
                //}

                if (model is WebRequestModel)
                {
                    if (actionContext.Request.Headers.TryGetValues("x-real-ip", out var values))
                    {
                        ((WebRequestModel)model).IP = values.FirstOrDefault();
                    }
                }
                // filter Merchant by user
                if(model is AllowedMerchant)
                {
                    var identity = HttpContext.Current.User.Identity as ClaimsIdentity;

                    if (identity != null)
                    {
                        var allowedMerchantIdString = identity.FindFirst("AllowedMerchantIds");

                        if (allowedMerchantIdString != null)
                        {
                            var allowedMerchantIdList = allowedMerchantIdString.Value.Split(',').Select(o => int.Parse(o)).ToList();
                            ((AllowedMerchant)model).AllowedMerchantIds = allowedMerchantIdList;
                        }
                    }
                }

                actionContext.ActionArguments[argument.ParameterName] = model;
            }

            await base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
    }
}
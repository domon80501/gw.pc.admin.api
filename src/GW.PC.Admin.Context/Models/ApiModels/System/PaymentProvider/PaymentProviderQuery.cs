﻿using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentProvider
{
    public class PaymentProviderQuery : AllowedMerchant
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("status")]
        public int? Status { get; set; }

        public Expression<Func<PC.Models.PaymentProvider, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.PaymentProvider>(true);

                if (!string.IsNullOrEmpty(Name))
                    query = query.And(x => x.Name.Contains(Name));

                if (Status.HasValue)
                    query = query.And(x => x.Status == (EntityStatus)Status);

                if (AllowedMerchantIds.Count > 0)
                    query = query.And(o => AllowedMerchantIds.Contains(o.MerchantId));

                return query;
            }
        }
    }
}

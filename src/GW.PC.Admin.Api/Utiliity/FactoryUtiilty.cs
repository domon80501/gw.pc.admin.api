﻿using GW.PC.Services.EF;
using GW.PC.Services.EF.Admin;
using GW.PC.Services.EF.Payment;
using GW.PC.Services.Queues.EF;

namespace GW.PC.Admin.Api.Utiliity
{
    public class FactoryUtiilty
    {
        public FactoryUtiilty()
        {
        }

        private MerchantService _merchantService;
        private UserService _userService;
        private RoleService _roleService;
        private EntityLogService _entityLogService;
        private PermissionService _permissionService;
        private PaymentChannelService _paymentChannelService;
        private BankService _bankService;
        private PaymentProviderService _paymentProviderService;
        private PaymentProviderBankService _paymentProviderBankService;
        private PaymentQueryRequestService _paymentQueryRequestService;
        private MerchantCallbackRequestService _merchantCallbackRequestService;
        private AlipayDepositService _alipayDepositService;
        //private OnlinePaymentDepositService _onlinePaymentDepositService;
        private WeChatDepositService _weChatDepositService;
        private OnlineBankingDepositService _onlineBankingDepositService;
        private PrepaidCardDepositService _prepaidCardDepositService;
        private QQWalletDepositService _qqWalletDepositService;
        private QuickPayDepositService _quickPayDepositService;
        private OnlineToBankCardDepositService _onlineToBankCardDepositService;
        private JDWalletDepositService _jdWalletdDepositService;
        private InternalDepositService _internalDepositService;
        private MerchantWithdrawalService _merchantWithdrawalService;

        #region Merchant
        public MerchantService MerchantService
        {
            get
            {
                if (_merchantService == null)
                {
                    _merchantService = new MerchantService();
                }
                return _merchantService;
            }
        }
        #endregion

        #region Order
        public MerchantCallbackRequestService MerchantCallbackRequestService
        {
            get
            {
                if (_merchantCallbackRequestService == null)
                {
                    _merchantCallbackRequestService = new MerchantCallbackRequestService();
                }
                return _merchantCallbackRequestService;
            }
        }
        public PaymentQueryRequestService PaymentQueryRequestService
        {
            get
            {
                if (_paymentQueryRequestService == null)
                {
                    _paymentQueryRequestService = new PaymentQueryRequestService();
                }
                return _paymentQueryRequestService;
            }
        }
        public AlipayDepositService AlipayDepositService
        {
            get
            {
                if (_alipayDepositService == null)
                {
                    _alipayDepositService = new AlipayDepositService();
                }
                return _alipayDepositService;
            }
        }
        //public OnlinePaymentDepositService OnlinePaymentDepositService
        //{
        //    get
        //    {
        //        if (_onlinePaymentDepositService == null)
        //        {
        //            _onlinePaymentDepositService = new OnlinePaymentDepositService();
        //        }
        //        return _onlinePaymentDepositService;
        //    }
        //}
        public WeChatDepositService WeChatDepositService
        {
            get
            {
                if (_weChatDepositService == null)
                {
                    _weChatDepositService = new WeChatDepositService();
                }
                return _weChatDepositService;
            }

        }
        public OnlineBankingDepositService OnlineBankingDepositService
        {
            get
            {
                if (_onlineBankingDepositService == null)
                {
                    _onlineBankingDepositService = new OnlineBankingDepositService();
                }
                return _onlineBankingDepositService;
            }
        }
        public PrepaidCardDepositService PrepaidCardDepositService
        {
            get
            {
                if (_prepaidCardDepositService == null)
                {
                    _prepaidCardDepositService = new PrepaidCardDepositService();
                }
                return _prepaidCardDepositService;
            }
        }
        public QQWalletDepositService QQWalletDepositService
        {
            get
            {
                if (_qqWalletDepositService == null)
                {
                    _qqWalletDepositService = new QQWalletDepositService();
                }
                return _qqWalletDepositService;
            }
        }
        public QuickPayDepositService QuickPayDepositService
        {
            get
            {
                if (_quickPayDepositService == null)
                {
                    _quickPayDepositService = new QuickPayDepositService();
                }
                return _quickPayDepositService;
            }
        }
        public OnlineToBankCardDepositService OnlineToBankCardDepositService
        {
            get
            {
                if (_onlineToBankCardDepositService == null)
                {
                    _onlineToBankCardDepositService = new OnlineToBankCardDepositService();
                }
                return _onlineToBankCardDepositService;
            }
        }
        public JDWalletDepositService JDWalletDepositService
        {
            get
            {
                if (_jdWalletdDepositService == null)
                {
                    _jdWalletdDepositService = new JDWalletDepositService();
                }
                return _jdWalletdDepositService;
            }

        }
        public InternalDepositService InternalDepositService
        {
            get
            {
                if (_internalDepositService == null)
                {
                    _internalDepositService = new InternalDepositService();
                }
                return _internalDepositService;
            }
        }
        public MerchantWithdrawalService MerchantWithdrawalService
        {
            get
            {
                if (_merchantWithdrawalService == null)
                {
                    _merchantWithdrawalService = new MerchantWithdrawalService();
                }
                return _merchantWithdrawalService;
            }
        }
        #endregion

        #region System
        public UserService UserService
        {
            get
            {
                if (_userService == null)
                {
                    _userService = new UserService();
                }
                return _userService;
            }
        }

        public RoleService RoleService
        {
            get
            {
                if (_roleService == null)
                {
                    _roleService = new RoleService();
                }
                return _roleService;
            }
        }

        public PermissionService PermissionService
        {
            get
            {
                if (_permissionService == null)
                {
                    _permissionService = new PermissionService();
                }
                return _permissionService;
            }
        }

        public PaymentChannelService PaymentChannelService
        {
            get
            {
                if (_paymentChannelService == null)
                {
                    _paymentChannelService = new PaymentChannelService();
                }
                return _paymentChannelService;
            }
        }

        public BankService BankService
        {
            get
            {
                if (_bankService == null)
                {
                    _bankService = new BankService();
                }
                return _bankService;
            }
        }

        public PaymentProviderService PaymentProviderService
        {
            get
            {
                if (_paymentProviderService == null)
                {
                    _paymentProviderService = new PaymentProviderService();
                }
                return _paymentProviderService;
            }
        }

        public PaymentProviderBankService PaymentProviderBankService
        {
            get
            {
                if (_paymentProviderBankService == null)
                {
                    _paymentProviderBankService = new PaymentProviderBankService();
                }
                return _paymentProviderBankService;
            }
        }

        public EntityLogService EntityLogService
        {
            get
            {
                if (_entityLogService == null)
                {
                    _entityLogService = new EntityLogService();
                }
                return _entityLogService;
            }
        }
        #endregion
    }
}

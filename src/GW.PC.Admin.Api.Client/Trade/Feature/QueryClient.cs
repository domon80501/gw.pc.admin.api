﻿using GW.PC.Web.Core;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade.Feature
{
    public class QueryClient : AdminApiClients
    {
        public QueryClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiTradeFeature;
            uri = "query";
        }

        public async Task<ApiResult> QueryDeposit(int id, int paymentMethod)
        {
            uri = $"{uri}?id={id}&paymentMethod={paymentMethod}";
            return await GetApiResultAsync(uri);
        }
    }
}

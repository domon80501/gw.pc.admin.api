﻿using GW.PC.Admin.Context.Models.ApiModels.System.Role;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.System
{
    public class RoleClient : AdminApiClients
    {
        public RoleClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystem;
            uri = "role";
        }

        public async Task<ApiResult<List<RoleListData>>> Search(RoleQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<RoleListData>>(uri);
        }

        public async Task<ApiResult<RoleDetail>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<RoleDetail>(uri);
        }

        public async Task<ApiResult> Create(RoleCreate req)
        {
            return await PostApiResultAsync(uri, req);
        }

        public async Task<ApiResult> Update(RoleUpdate req)
        {
            return await PutApiResultAsync(uri, req);
        }

        public async Task<ApiResult> UpdateStatus(RoleStatus req)
        {
            return await PatchApiResultAsync(uri, req);
        }
    }
}

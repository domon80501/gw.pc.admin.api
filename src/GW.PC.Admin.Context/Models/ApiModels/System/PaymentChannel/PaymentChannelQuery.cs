﻿using GW.PC.Core;
using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentChannel
{
    public class PaymentChannelQuery : AllowedMerchant
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("providerId")]
        public int? ProviderId { get; set; }
        [JsonProperty("paymentPlatform")]
        public int? PaymentPlatform { get; set; }
        [JsonProperty("paymentType")]
        public int? PaymentType { get; set; }
        [JsonProperty("paymentMethod")]
        public int? PaymentMethod { get; set; }
        [JsonProperty("status")]
        public int? Status { get; set; }
        [JsonProperty("merchantId")]
        public int? MerchantId { get; set; }

        public Expression<Func<PC.Models.PaymentChannel, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.PaymentChannel>(true);

                if (!string.IsNullOrEmpty(Name))
                    query = query.And(x => x.Name.Contains(Name));

                if (PaymentType.HasValue)
                    query = query.And(o => o.PaymentType == (PaymentType)PaymentType.Value);

                if (PaymentMethod.HasValue)
                    query = query.And(o => o.PaymentMethod == (PaymentMethod)PaymentMethod.Value);

                if (PaymentPlatform.HasValue)
                    query = query.And(o => o.PaymentPlatform == (PaymentPlatform)PaymentPlatform.Value);

                if (ProviderId.HasValue)
                    query = query.And(o => o.ProviderId == ProviderId.Value);

                if (MerchantId.HasValue)
                    query = query.And(o => o.MerchantId == MerchantId.Value);

                if (Status.HasValue)
                    query = query.And(o => o.Status == (EntityStatus)Status);

                if (AllowedMerchantIds.Count > 0)
                    query = query.And(o => AllowedMerchantIds.Contains(o.MerchantId));

                return query;
            }
        }
    }
}

﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentChannel
{
    public class PaymentChannelDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("merchantId")]
        public int MerchantId { get; set; }
        [JsonProperty("paymentType")]
        public int PaymentType { get; set; }
        [JsonProperty("paymentMethod")]
        public int PaymentMethod { get; set; }
        [JsonProperty("paymentProviderId")]
        public int PaymentProviderId { get; set; }
        [JsonProperty("commissionPercentage")]
        public decimal? CommissionPercentage { get; set; }
        [JsonProperty("merchantUsername")]
        public string MerchantUsername { get; set; }
        [JsonProperty("merchantKey")]
        public string MerchantKey { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("paymentPlatform")]
        public int PaymentPlatform { get; set; }
        [JsonProperty("commissionConstants")]
        public decimal? CommissionConstants { get; set; }
        [JsonProperty("MaxAmount")]
        public decimal MaxAmount { get; set; }
        [JsonProperty("MinAmount")]
        public decimal MinAmount { get; set; }
        [JsonProperty("totalLimit")]
        public decimal TotalLimit { get; set; }
        [JsonProperty("minimumWithdrawInterval")]
        public decimal? MinimumWithdrawInterval { get; set; }
        [JsonProperty("commissionMinValue")]
        public decimal? CommissionMinValue { get; set; }
        [JsonProperty("requestUrl")]
        public string RequestUrl { get; set; }
        [JsonProperty("callbackUrl")]
        public string CallbackUrl { get; set; }
        [JsonProperty("queryUrl")]
        public string QueryUrl { get; set; }
        [JsonProperty("handlerType")]
        public string HandlerType { get; set; }
        [JsonProperty("assemblyName")]
        public string AssemblyName { get; set; }
        [JsonProperty("arguments")]
        public string Arguments { get; set; }
        [JsonProperty("notes")]
        public string Notes { get; set; }
        [JsonProperty("extensionRoute")]
        public int ExtensionRoute { get; set; }
        [JsonProperty("callbackSuccessType")]
        public int CallbackSuccessType { get; set; }
    }
}

﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.System.PaymentProvider;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.System
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystem)]
    public class PaymentProviderController : BaseController
    {
        [Route("paymentProvider")]
        public void Options()
        { }

        [Route("paymentProvider")]
        [CustomAuthorization(Permission = PermissionCode.System_PaymentProvider_Access)]
        public async Task<IEnumerable<PaymentProviderData>> Get([FromUri]PaymentProviderQuery req)
        {

            var paymentProviders = await factory.PaymentProviderService.QueryAsync(req.Content);
            return paymentProviders.Select(s => new PaymentProviderData
            {
                Id = s.Id,
                Name = s.Name,
                Status = s.Status,
                CreatedById = s.CreatedBy?.Username,
                CreatedAt = s.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss")
            }).ToList();
        }

        [Route("paymentProvider")]
        public async Task<PaymentProviderDTO> Get(int id)
        {
            var paymentProvider = await factory.PaymentProviderService.Find(id);
            return new PaymentProviderDTO()
            {
                Id = paymentProvider.Id,
                Name = paymentProvider.Name,
                Status = paymentProvider.Status,
                CreatedById = paymentProvider.CreatedBy?.Username,
                CreatedAt = paymentProvider.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss")
            };
        }

        [Route("paymentProvider")]
        [CustomAuthorization(Permission = PermissionCode.System_PaymentProvider_Create)]
        public async Task Post([FromBody]PaymentProviderDTO req)
        {
            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            PaymentProvider paymentProvider = new PaymentProvider
            {
                Name = req.Name,
                Status = EntityStatus.Enabled,
                MerchantId = req.MerchantId,
                CreatedAt = DateTime.Now.ToE8(),
                CreatedById = createId
            };
            await factory.PaymentProviderService.Save(paymentProvider);
        }

        [Route("paymentProvider")]
        [CustomAuthorization(Permission = PermissionCode.System_PaymentProvider_Detail_Edit)]
        public async Task Put([FromBody]PaymentProviderDTO req)
        {
            PaymentProvider paymentProvider = await factory.PaymentProviderService.Find(req.Id);
            paymentProvider.Name = req.Name;
            await factory.PaymentProviderService.Save(paymentProvider);
        }

        [Route("paymentProvider")]
        [CustomAuthorization(Permission = PermissionCode.System_PaymentProvider_Detail_Status_Edit)]
        public async Task Patch(PaymentProviderStatus req)
        {
            PaymentProvider paymentProvider = await factory.PaymentProviderService.Find(req.Id);

            paymentProvider.Status = req.Status;

            await factory.PaymentProviderService.Save(paymentProvider);
        }
    }
}
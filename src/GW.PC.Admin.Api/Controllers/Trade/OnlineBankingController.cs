﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineBanking;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class OnlineBankingController : BaseController
    {
        [Route("onlinebanking")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<OnlineBankingData>> Get([FromUri]OnlineBankingQuery req)
        {
            var onlineBankingDeposits = await factory.OnlineBankingDepositService.QueryAsync(req.Content, "Merchant", "PaymentChannel", "PaymentProviderBank.Bank");

            var result = onlineBankingDeposits.Select(o => new OnlineBankingData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                BankName = o.PaymentProviderBank.Bank.Name,
                PayerName = o.PayerName,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        [Route("onlinebanking")]
        public async Task<OnlineBankingDTO> Get(int id)
        {
            var onlineBanking = await factory.OnlineBankingDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy", "PaymentProviderBank.Bank");

            var result = new OnlineBankingDTO()
            {
                Id = onlineBanking.Id,
                MerchantName = onlineBanking.Merchant.Name,
                CustomerUserName = onlineBanking.Username,
                BankName = onlineBanking.PaymentProviderBank.Bank.Name,
                DepositIP = onlineBanking.DepositIP,
                Amount = onlineBanking.RequestedAmount,
                ActualAmount = onlineBanking.ActualAmount , 
                PayeeName = onlineBanking.PayeeName,
                PayerName = onlineBanking.PayerName,
                PaymentChannelName = onlineBanking.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(onlineBanking.Status),
                Commission = onlineBanking.Commission,
                DepositAddress = onlineBanking.DepositAddress,
                CreateAt = onlineBanking.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = onlineBanking.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = onlineBanking.MerchantOrderNumber,
                SystemOrderNumber = onlineBanking.SystemOrderNumber,
                PaymentCenterOrderNumber = onlineBanking.PaymentCenterOrderNumber,
                PaymentOrderNumber = onlineBanking.PaymentOrderNumber,
                ManualApprovedAt = onlineBanking.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = onlineBanking.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
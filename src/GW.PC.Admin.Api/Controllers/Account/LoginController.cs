﻿using GW.PC.Admin.Context.Models.ApiModels.Account;
using GW.PC.Web.Core;
using GW.PC.Models;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using GW.PC.Core;
using Newtonsoft.Json;

namespace GW.PC.Admin.Api.Controllers.Account
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiAccount)]
    public class LoginController : BaseController
    {
        [Route("login")]
        public void Options()
        { }

        [Route("login")]
        [AllowAnonymous]
        public async Task Post(LoginDTO req)
        {
            User user = await factory.UserService.Login(req.UserName, req.Password);
            user = await factory.UserService.Find(user.Id, "Roles", "Merchants");

            List<Claim> claims = new List<Claim>();

            #region Permission Claims
            List<string> permimssionCodes = new List<string>();
            foreach (var ro in user.Roles)
            {
                var role = await factory.RoleService.Find(ro.Id, "Permissions");

                if (role != null)
                {
                    claims.Add(new Claim(ClaimTypes.Role, ro.Name));

                    if (role.Permissions != null)
                    {
                        foreach (var permission in role.Permissions)
                        {
                            if (!claims.Any(o => o.Type == "Permission" && o.Value == permission.Code.ToString()))
                            {
                                claims.Add(new Claim("Permission", permission.Code.ToString()));
                            }
                        }
                    }
                }
            }
            #endregion

            #region AllowedMerchants Claims
            //AllowedMerchantIds
            if (user.Merchants != null)
            {
                var merchantIds = string.Join(",", user.Merchants.Select(o => o.Id).ToArray());
                claims.Add(new Claim("AllowedMerchantIds", merchantIds));
            }
            #endregion

            // 储存使用者资讯
            ClaimsIdentity userIdentity = new ClaimsIdentity(
                claims, DefaultAuthenticationTypes.ApplicationCookie);

            HttpContext.Current.GetOwinContext().Authentication.SignIn(userIdentity);


            //var user1 = JsonConvert.SerializeObject(HttpContext.Current.GetOwinContext().Authentication.User);
            //SerilogLogger.Logger.LogInformation($"TimmyAuth: owin: {user1}");


            //var aa = HttpContext.Current.GetOwinContext().Authentication.User.Identities;

            //foreach(var a in aa)
            //{
            //    var aJson = JsonConvert.SerializeObject(a);
            //    SerilogLogger.Logger.LogInformation($"TimmyAuth: owinIdentity: {aJson}");
            //}


          
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;

            //var identityJson = JsonConvert.SerializeObject(identity.Claims);
            //SerilogLogger.Logger.LogInformation($"TimmyAuth: identity: {identityJson}");

            //if (identity != null)
            //{
            //    var permissionJson = JsonConvert.SerializeObject(identity.FindAll("Permission"));
            //    SerilogLogger.Logger.LogInformation($"Timmy Permission: permissions: {permissionJson}");
            //}
        }

        [Route("login")]
        public async Task Delete()
        {
            HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

    }
}

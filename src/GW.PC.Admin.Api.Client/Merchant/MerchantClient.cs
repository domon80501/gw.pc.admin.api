﻿using GW.PC.Admin.Context.Models.ApiModels.Merchants.Merchant;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Merchant
{
    public class MerchantClient : AdminApiClients
    {
        public MerchantClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiMerchant;
            uri = "merchant";
        }

        public async Task<ApiResult<List<MerchantData>>> Search(MerchantQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<MerchantData>>(uri);
        }

        public async Task<ApiResult<MerchantDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<MerchantDTO>(uri);
        }

        public async Task<ApiResult> Create(MerchantDTO req)
        {
            return await PostApiResultAsync(uri, req);
        }

        public async Task<ApiResult> Update(MerchantDTO req)
        {
            return await PutApiResultAsync(uri, req);
        }

        public async Task<ApiResult> UpdateStatus(MerchantStatus req)
        {
            return await PatchApiResultAsync(uri, req);
        }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GW.PC.Admin.Context.Models.ApiModels.System.Role
{
    public class RoleListData
    {
        public RoleListData()
        {
            Permissions = new List<SubPermission>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("roleName")]
        public string RoleName { get; set; }
        [JsonProperty("createdByUserName")]
        public string CreatedByUserName { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("subPermissions")]
        public List<SubPermission> Permissions { get; set; }
    }

    public class SubPermission
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Merchants.Merchant
{
    public class MerchantDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("userName")]
        public string UserName { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("merchantKey")]
        public string MerchantKey { get; set; }
        [JsonProperty("notes")]
        public string Notes { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("createdByUserName")]
        public string CreatedByUserName { get; set; }
    }
}

﻿using GW.PC.Models;
using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentProvider
{
    public class PaymentProviderStatus
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("status")]
        public EntityStatus Status { get; set; }
    }
}

﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.Wechat;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class WeChatController : BaseController
    {
        [Route("wechat")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<WeChatData>> Get([FromUri]WeChatQuery req)
        {
            var weChatDeposits = await factory.WeChatDepositService.QueryAsync(req.Content, "Merchant", "PaymentChannel");

            var result = weChatDeposits.Select(o => new WeChatData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        [Route("wechat")]
        public async Task<WeChatDTO> Get(int id)
        {
            var weChat = await factory.WeChatDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new WeChatDTO()
            {
                Id = weChat.Id,
                MerchantName = weChat.Merchant.Name,
                CustomerUserName = weChat.Username,
                DepositIP = weChat.DepositIP,
                Amount = weChat.ActualAmount.HasValue ? weChat.ActualAmount : weChat.RequestedAmount,
                PaymentChannelName = weChat.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(weChat.Status),
                Commission = weChat.Commission,
                DepositAddress = weChat.DepositAddress,
                CreateAt = weChat.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = weChat.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = weChat.MerchantOrderNumber,
                SystemOrderNumber = weChat.SystemOrderNumber,
                PaymentCenterOrderNumber = weChat.PaymentCenterOrderNumber,
                PaymentOrderNumber = weChat.PaymentOrderNumber,
                ManualApprovedAt = weChat.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = weChat.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
﻿using GW.PC.Models;
using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentProviderBank
{
    public class PaymentProviderBankStatus
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("status")]
        public EntityStatus Status { get; set; }
    }
}

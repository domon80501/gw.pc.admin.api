﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade.MerchantWithdrawal
{
    public class MerchantWithdrawalData : TradeBase
    {
        [JsonProperty("customerUserName")]
        public string CustomerUserName { get; set; }
        [JsonProperty("merchantOrderNumber")]
        public string MerchantOrderNumber { get; set; }
        [JsonProperty("payeeName")]
        public string PayeeName { get; set; }
        [JsonProperty("requestedAmount")]
        public string Amount { get; set; }
        [JsonProperty("commission")]
        public string Commission { get; set; }
        [JsonProperty("bankName")]
        public string BankName { get; set; }
        [JsonProperty("payeeCardNumber")]
        public string PayeeCardNumber { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("auditedAt")]
        public string AuditedAt { get; set; }
        [JsonProperty("withdrawalStatus")]
        public string WithdrawalStatus { get; set; }
    }
}

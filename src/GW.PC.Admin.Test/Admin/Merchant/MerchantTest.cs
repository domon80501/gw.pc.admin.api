﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.Merchant;
using GW.PC.Admin.Context.Models.ApiModels.Merchants.Merchant;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GW.PC.UnitTest.Admin.Merchant
{
    [TestClass]
    public class MerchantTest
    {
        private MerchantClient _client;
        public MerchantClient MerchantClient
        {
            get
            {
                if (_client == null)
                    _client = new MerchantClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestPostMethod()
        {
            try
            {
                var userName = "TestMerchant_" + RandomGenerator.RandomString(10);

                bool isExist = await IsExistMerchant(userName);
                while (isExist)
                {
                    userName = "TestMerchant_" + RandomGenerator.RandomString(10);
                    isExist = await IsExistMerchant(userName);
                }

                MerchantDTO req = new MerchantDTO()
                {
                    Name = userName,
                    UserName = userName,
                    MerchantKey = "TestKey",
                    Notes = "testNote",
                    Status = (int)EntityStatus.Enabled,
                };

                var result = await MerchantClient.Create(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private async Task<bool> IsExistMerchant(string userName)
        {
            var merchantList = await new MerchantService().GetAll();
            var merchant = merchantList.FirstOrDefault(o => o.Username == userName);

            return merchant != null;
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                MerchantQuery req = new MerchantQuery();

                //req.MerchantName = "蜻";

                var result = await MerchantClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            try
            {
                var mcList = await new MerchantService().GetAll();
                Models.Merchant mc = mcList.FirstOrDefault(o => o.Username.Contains("TestMerchant"));

                while (mc == null)
                {
                    await TestPostMethod();

                    mcList = await new MerchantService().GetAll();
                    mc = mcList.FirstOrDefault(o => o.Username.Contains("TestMerchant"));
                }

                int id = mc.Id;
                var result = await MerchantClient.FindById(id);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        

        [TestMethod]
        public async Task TestPutMethod()
        {
            try
            {
                var mcList = await new MerchantService().GetAll();
                Models.Merchant mc = mcList.FirstOrDefault(o => o.Username.Contains("TestMerchant"));

                while(mc == null)
                {
                    await TestPostMethod();

                    mcList = await new MerchantService().GetAll();
                    mc = mcList.FirstOrDefault(o => o.Username.Contains("TestMerchant"));
                }

                MerchantDTO req = new MerchantDTO()
                {
                    Id = mc.Id,
                    Name = "PutTest",
                    UserName = mc.Username,
                    MerchantKey = "PutTest",
                    Notes = "PutTest",
                    Status = (int)EntityStatus.Deprecated,
                };

                var result = await MerchantClient.Update(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestPatchMethod()
        {
            try
            {
                var mcList = await new MerchantService().GetAll();
                Models.Merchant mc = mcList.FirstOrDefault(o => o.Username.Contains("TestMerchant"));

                while (mc == null)
                {
                    await TestPostMethod();

                    mcList = await new MerchantService().GetAll();
                    mc = mcList.FirstOrDefault(o => o.Username.Contains("TestMerchant"));
                }

                MerchantStatus req = new MerchantStatus()
                {
                    Id = mc.Id,
                    Status = (int)EntityStatus.Deprecated,
                };

                var result = await MerchantClient.UpdateStatus(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}

﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.QuickPay;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class QuickPayController : BaseController
    {
        [Route("quickpay")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<QuickPayData>> Get([FromUri]QuickPayQuery req)
        {
            var quickPayDeposits = await factory.QuickPayDepositService.QueryAsync(req.Content, "Merchant", "PaymentChannel");

            var result = quickPayDeposits.Select(o => new QuickPayData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        [Route("quickpay")]
        public async Task<QuickPayDTO> Get(int id)
        {
            var quickPay = await factory.QuickPayDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new QuickPayDTO()
            {
                Id = quickPay.Id,
                MerchantName = quickPay.Merchant.Name,
                CustomerUserName = quickPay.Username,
                DepositIP = quickPay.DepositIP,
                Amount = quickPay.ActualAmount.HasValue ? quickPay.ActualAmount : quickPay.RequestedAmount,
                PaymentChannelName = quickPay.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(quickPay.Status),
                Commission = quickPay.Commission,
                DepositAddress = quickPay.DepositAddress,
                CreateAt = quickPay.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = quickPay.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = quickPay.MerchantOrderNumber,
                SystemOrderNumber = quickPay.SystemOrderNumber,
                PaymentCenterOrderNumber = quickPay.PaymentCenterOrderNumber,
                PaymentOrderNumber = quickPay.PaymentOrderNumber,
                ManualApprovedAt = quickPay.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = quickPay.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
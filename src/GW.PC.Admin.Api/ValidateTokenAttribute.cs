﻿using AP.Services.EF;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace GW.PC.Admin.Api
{
    public class ValidateTokenAttribute : ActionFilterAttribute
    {
        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (actionContext.ActionArguments["model"] is TokenizedRequestModel model)
            {
                model.Username = await new CustomerService().ValidateLoginToken(model.Token);
            }

            await base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
    }
}
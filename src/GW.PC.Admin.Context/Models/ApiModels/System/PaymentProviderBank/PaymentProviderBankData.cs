﻿using GW.PC.Models;
using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentProviderBank
{
    public class PaymentProviderBankData
    {
		[JsonProperty("id")]
        public int Id { get; set; }
		[JsonProperty("paymentProviderBankName")]
        public string PaymentProviderBankName { get; set; }
		[JsonProperty("paymentProviderName")]
        public string PaymentProviderName { get; set; }
		[JsonProperty("paymentTypeName")]
        public string PaymentTypeName { get; set; }
		[JsonProperty("paymentMethodName")]
        public string PaymentMethodName { get; set; }
		[JsonProperty("paymentProviderBankCode")]
        public string PaymentProviderBankCode { get; set; }
        [JsonProperty("order")]
        public int Order { get; set; }
        [JsonProperty("status")]
        public EntityStatus Status { get; set; }
    }
}

﻿using GW.PC.Admin.Context.Models.ApiModels.SystemExtension.PermissionGroup;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.SystemExtension
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystemExtension)]
    public class PermissionGroupController : BaseController
    {
        [Route("permissiongroup")]
        public async Task<IEnumerable<PermissionListData>> Get()
        {
            var permissionGroups = await factory.PermissionService.GetGroups();

            var responseGroup = permissionGroups.Select(o => new PermissionListData()
            {
                Id = o.Id,
                Name = o.Name,
                ParentId = o.ParentId,
                Permissions = o.Permissions
                    .Where(r => r.Status == EntityStatus.Enabled)
                    .Select(x => new SubPermission()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        MasterId = o.Id,
                    }).ToList()
            }).ToList();

            return await BuildTree(responseGroup);
        }

        #region private method
        private Task<List<PermissionListData>> BuildTree(List<PermissionListData> sourceData)
        {
            int i = sourceData.Count() - 1;

            while (i > -1)
            {
                var menu = sourceData[i];
                var currentMenu = new PermissionListData()
                {
                    Id = menu.Id,
                    Name = menu.Name,
                    ParentId = menu.ParentId,
                    Permissions = menu.Permissions,
                };

                // 遍尋所有已在清單中的目錄
                foreach (var resultMenu in sourceData)
                {
                    var findResult = FindParent(resultMenu, currentMenu);

                    if (findResult != null)
                    {
                        findResult.SubPermissionGroups.Add(currentMenu);
                        findResult.SubPermissionGroups = findResult.SubPermissionGroups.OrderBy(p => p.Id).ToList();
                        sourceData.Remove(sourceData.First(p => p.Id == currentMenu.Id));
                        break;
                    }
                }
                i--;
            }
            return Task.FromResult(sourceData);
        }

        private PermissionListData FindParent(PermissionListData menu, PermissionListData compareMenu)
        {
            if (!compareMenu.ParentId.HasValue)
                return null;

            if (menu.Id == compareMenu.ParentId)
            {
                return menu;
            }
            foreach (var subMenu in menu.SubPermissionGroups)
            {
                var parent = FindParent(subMenu, compareMenu);
                if (parent != null)
                {
                    return parent;
                }
            }
            return null;
        }
        #endregion
    }
}
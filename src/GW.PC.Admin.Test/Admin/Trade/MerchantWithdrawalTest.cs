﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.MerchantWithdrawal;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Trade
{
    [TestClass]
    public class MerchantWithdrawalTest
    {
        private MerchantWithdrawalClient _client;
        public MerchantWithdrawalClient MerchantWithdrawalClient
        {
            get
            {
                if (_client == null)
                    _client = new MerchantWithdrawalClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                MerchantWithdrawalQuery req = new MerchantWithdrawalQuery();

                var result = await MerchantWithdrawalClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            try
            {
                var withdrawalList = await new MerchantWithdrawalService().GetAll();
                MerchantWithdrawal withdrawal = withdrawalList.FirstOrDefault(o => o.Username.Contains("TestWithdrawal"));

                while (withdrawal == null)
                {
                    await InsertWithdrawalAsync();

                    withdrawalList = await new MerchantWithdrawalService().GetAll();
                    withdrawal = withdrawalList.FirstOrDefault(o => o.Username.Contains("TestWithdrawal"));
                }

                int id = withdrawal.Id;
                var result = await MerchantWithdrawalClient.FindById(id);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task InsertWithdrawalAsync()
        {
            MerchantWithdrawal withdrawal = new MerchantWithdrawal();

            withdrawal.Username = "TestWithdrawal" + RandomGenerator.RandomString(10);
            withdrawal.PayeeCardNumber = "payeecardNumber";
            withdrawal.PayeeName = "PayeeName";
            withdrawal.PayeeCardAddress = "PayeeCardAddress";
            withdrawal.Amount = 100;
            withdrawal.WithdrawalIP = "127.0.0.1";
            withdrawal.WithdrawalAddress = "withdrawalAdd";
            withdrawal.PayeeBankId = 3;
            withdrawal.PaidById = null;
            withdrawal.PaidAt = DateTime.Now;
            withdrawal.Status = WithdrawalStatus.AutoPaymentInProgress;
            withdrawal.AuditedById = 1;
            withdrawal.AuditedAt = DateTime.Now;
            withdrawal.CreatedAt = DateTime.Now.AddDays(-1);
            withdrawal.ProcessingTime = 4;
            withdrawal.DeclineNotes = "delineNotes";
            withdrawal.Commission = 10;
            withdrawal.PaymentChannelId = 1;
            withdrawal.MerchantOrderNumber = "merchantOrdernumber";
            withdrawal.ManualConfirmedById = 1;
            withdrawal.ManualConfirmedAt = DateTime.Now;
            withdrawal.ManualConfirmNotes = "ManualConfirmNotes";
            withdrawal.MerchantMessage = "MerchantMessage";
            withdrawal.Parent = null;
            withdrawal.HasChildren = false;
            withdrawal.ReferenceId = null;
            withdrawal.SystemOrderNumber = "systemOrderNumber";
            withdrawal.PaymentOrderNumber = "PaymentOrderNumber";
            withdrawal.PaymentCenterOrderNumber = "PaymentCenterOrderNumber";
            withdrawal.MerchantId = 1;
            withdrawal.CallbackUrl = "http://google.com";
            withdrawal.MerchantCallbackStatus = MerchantCallbackStatus.Await;

            await new MerchantWithdrawalService().Add(withdrawal);
        }

    }
}

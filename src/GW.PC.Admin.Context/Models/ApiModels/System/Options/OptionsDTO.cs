﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.Options
{
    public class OptionsDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

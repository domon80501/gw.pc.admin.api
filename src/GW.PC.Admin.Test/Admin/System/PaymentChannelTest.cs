﻿using System;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.System;
using GW.PC.Admin.Context.Enum;
using GW.PC.Admin.Context.Models.ApiModels.System.PaymentChannel;
using GW.PC.Core;
using GW.PC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using GW.PC.Services.EF;

namespace GW.PC.UnitTest.Admin.System
{
    [TestClass]
    public class PaymentChannelTest
    {
        private PaymentChannelClient _client;
        public PaymentChannelClient PaymentChannelClient
        {
            get
            {
                if (_client == null)
                    _client = new PaymentChannelClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestPostMethod()
        {
            try
            {
                int[] roleIds = new[] { 1, 2, 3, 4 };

                var name = "TestPaymentChannels_" + RandomGenerator.RandomString(10);

                bool isExist = await IsExistChannel(name);
                while (isExist)
                {
                    name = "TestPaymentChannels_" + RandomGenerator.RandomString(10);
                    isExist = await IsExistChannel(name);
                }


                PaymentChannelDTO req = new PaymentChannelDTO()
                {
                    Name = name,
                    MerchantId = 1,
                    PaymentType = 1,
                    PaymentMethod = 1,
                    PaymentProviderId = 1,
                    CommissionPercentage = 10,
                    MerchantUsername = "who",
                    MerchantKey = "123456",
                    Status = 1,
                    PaymentPlatform = 1,
                    CommissionConstants = 50,
                    MaxAmount = 50,
                    MinAmount = 50,
                    TotalLimit = 50,
                    MinimumWithdrawInterval = 50,
                    CommissionMinValue = 50,
                    RequestUrl = "1",
                    CallbackUrl = "1",
                    QueryUrl = "1",
                    AssemblyName = "QT",
                    Arguments = "argument",
                    HandlerType = "1",
                    Notes = "",
                    ExtensionRoute = 1,
                    CallbackSuccessType = 1,
                };

                var result = await PaymentChannelClient.Create(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private async Task<bool> IsExistChannel(string name)
        {
            var channelList = await new PaymentChannelService().GetAll();
            var channel = channelList.FirstOrDefault(o => o.Name == name);

            return channel != null;
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                PaymentChannelQuery req = new PaymentChannelQuery();

                //req.Name = "";
                //req.ProviderId = 0;
                //req.PaymentPlatform = 0;
                //req.PaymentType = 0;
                //req.PaymentMethod = 0;
                //req.Status = 0;

                var result = await PaymentChannelClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            try
            {
                var pcList = await new PaymentChannelService().GetAll();
                PaymentChannel pc = pcList.FirstOrDefault(o => o.Name.Contains("TestPaymentChannels_"));

                while (pc == null)
                {
                    await TestPostMethod();

                    pcList = await new PaymentChannelService().GetAll();
                    pc = pcList.FirstOrDefault(o => o.Name.Contains("TestPaymentChannels_"));
                }

                int id = pc.Id;
                var result = await PaymentChannelClient.FindById(id);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [TestMethod]
        public async Task TestGetSuccessRate()
        {
            try
            {
                PaymentChannelSuccessRateQuery req = new PaymentChannelSuccessRateQuery();

                req.Id = 1;
                req.SuccessRateDateType = (int)DateType.Day;
                req.Date = "2018/11/11";
                req.Month = "2018/11";

                var result = await PaymentChannelClient.GetSuccessRate(req, "successRate");

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [TestMethod]
        public async Task TestPutMethod()
        {
            try
            {
                var pcList = await new PaymentChannelService().GetAll();
                PaymentChannel pc = pcList.FirstOrDefault(o => o.Name.Contains("TestPaymentChannels_"));

                while (pc == null)
                {
                    await TestPostMethod();

                    pcList = await new PaymentChannelService().GetAll();
                    pc = pcList.FirstOrDefault(o => o.Name.Contains("TestPaymentChannels_"));
                }

                PaymentChannelDTO req = new PaymentChannelDTO()
                {
                    Id = pc.Id,
                    Name = pc.Name,
                    MerchantId = pc.MerchantId,
                    PaymentType = (int)pc.PaymentType,
                    PaymentMethod = (int)pc.PaymentMethod,
                    PaymentProviderId = 1,
                    CommissionPercentage = 33,
                    MerchantUsername = pc.MerchantUsername,
                    MerchantKey = pc.MerchantKey,
                    PaymentPlatform = (int)pc.PaymentPlatform,
                    CommissionConstants = pc.CommissionConstants,
                    MaxAmount = pc.MaxAmount,
                    MinAmount = pc.MinAmount,
                    TotalLimit = pc.TotalLimit,
                    MinimumWithdrawInterval = pc.MinimumWithdrawInterval,
                    CommissionMinValue = pc.CommissionMinValue,
                    RequestUrl = pc.RequestUrl,
                    CallbackUrl = pc.CallbackUrl,
                    QueryUrl = pc.QueryUrl,
                    AssemblyName = pc.AssemblyName,
                    Arguments = pc.Arguments,
                    HandlerType = pc.HandlerType,
                    Notes = pc.Notes,
                    ExtensionRoute = (int)pc.ExtensionRoute,
                    CallbackSuccessType = (int)pc.CallbackSuccessType,
                };

                var result = await PaymentChannelClient.Update(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestPatchMethod()
        {
            try
            {
                var pcList = await new PaymentChannelService().GetAll();
                PaymentChannel pc = pcList.FirstOrDefault(o => o.Name.Contains("TestPaymentChannels_"));

                while (pc == null)
                {
                    await TestPostMethod();

                    pcList = await new PaymentChannelService().GetAll();
                    pc = pcList.FirstOrDefault(o => o.Name.Contains("TestPaymentChannels_"));
                }

                PaymentChannelStatus req = new PaymentChannelStatus()
                {
                    Id = pc.Id,
                    //Status = (int)EntityStatus.Disabled,
                    //Status = (int)EntityStatus.Enabled,
                    Status = (int)EntityStatus.Deprecated,
                };

                var result = await PaymentChannelClient.UpdateStatus(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}

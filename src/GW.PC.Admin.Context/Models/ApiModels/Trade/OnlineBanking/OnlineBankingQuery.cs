﻿using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineBanking
{
    public class OnlineBankingQuery : AllowedMerchant
    {
        [JsonProperty("merchantId")]
        public int MerchantId { get; set; }
        [JsonProperty("customerUserName")]
        public string CustomerUserName { get; set; }
        [JsonProperty("paymentCenterOrderNumber")]
        public string PaymentCenterOrderNumber { get; set; }
        [JsonProperty("merchantOrderNumber")]
        public string MerchantOrderNumber { get; set; }
        [JsonProperty("depositStatus")]
        public DepositStatus? DepositStatus { get; set; }
        [JsonProperty("createAtStart")]
        public DateTime? CreateAtStart { get; set; }
        [JsonProperty("createAtEnd")]
        public DateTime? CreateAtEnd { get; set; }

        public Expression<Func<OnlineBankingDeposit, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<OnlineBankingDeposit>(true);

                if (MerchantId != 0)
                    query = query.And(o => o.MerchantId == MerchantId);

                if (!string.IsNullOrEmpty(CustomerUserName))
                    query = query.And(o => o.Username.Contains(CustomerUserName));

                if (!string.IsNullOrEmpty(PaymentCenterOrderNumber))
                    query = query.And(o => o.PaymentCenterOrderNumber.Contains(PaymentCenterOrderNumber));

                if (!string.IsNullOrEmpty(MerchantOrderNumber))
                    query = query.And(o => o.MerchantOrderNumber.Contains(MerchantOrderNumber));

                if (DepositStatus.HasValue)
                    query = query.And(o => o.Status == DepositStatus.Value);

                if (CreateAtStart.HasValue)
                    query = query.And(o => o.CreatedAt >= CreateAtStart.Value);

                if (CreateAtEnd.HasValue)
                    query = query.And(o => o.CreatedAt <= CreateAtEnd.Value);

                if (AllowedMerchantIds.Count > 0)
                    query = query.And(o => AllowedMerchantIds.Contains(o.MerchantId.Value));

                return query;
            }
        }
    }
}

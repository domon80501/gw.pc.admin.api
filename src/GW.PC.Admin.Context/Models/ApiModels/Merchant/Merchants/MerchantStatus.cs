﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Merchants.Merchant
{
    public class MerchantStatus
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
    }
}

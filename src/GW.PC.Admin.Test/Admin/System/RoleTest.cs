﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.System;
using GW.PC.Admin.Context.Models.ApiModels.System.Role;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF.Admin;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GW.PC.UnitTest
{
    [TestClass]
    public class RoleTest
    {
        private RoleClient _client;
        public  RoleClient RoleClient
        {
            get
            {
                if (_client == null)
                    _client = new RoleClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestPostMethod()
        {
            try
            {
                List<int> perms = new List<int>();
                perms.Add(1);

                var roleName = "TestRole_" + RandomGenerator.RandomString(10);

                bool isExist = await IsExistRole(roleName);
                while (isExist)
                {
                    roleName = "TestRole_" + RandomGenerator.RandomString(10);
                    isExist = await IsExistRole(roleName);
                }

                RoleCreate req = new RoleCreate()
                {
                    RoleName = roleName,
                    PermissionIds = perms,
                };

                var result = await RoleClient.Create(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private async Task<bool> IsExistRole(string roleName)
        {
            var roleList = await new RoleService().GetAll();
            var role = roleList.FirstOrDefault(o => o.Name == roleName);

            return role != null;
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                RoleQuery req = new RoleQuery();
                var result = await RoleClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            try
            {
                var roleList = await new RoleService().GetAll();
                Role role = roleList.FirstOrDefault(o => o.Name.Contains("TestRole_"));

                while (role == null)
                {
                    await TestPostMethod();

                    roleList = await new RoleService().GetAll();
                    role = roleList.FirstOrDefault(o => o.Name.Contains("TestRole_"));
                }

                int id = role.Id;
                var result = await RoleClient.FindById(id);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        
        }
        
        [TestMethod]
        public async Task TestPutMethod()
        {
            try
            {
                var roleList = await new RoleService().GetAll();
                Role role = roleList.FirstOrDefault(o => o.Name.Contains("TestRole_"));

                while (role == null)
                {
                    await TestPostMethod();

                    roleList = await new RoleService().GetAll();
                    role = roleList.FirstOrDefault(o => o.Name.Contains("TestRole_"));
                }

                RoleUpdate req = new RoleUpdate()
                {
                    Id = role.Id,
                    RoleName = "testTwiceRole",
                };
                req.PermissionIds.Add(5);
                req.PermissionIds.Add(6);

                var result = await RoleClient.Update(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestPatchMethod()
        {
            try
            {
                var roleList = await new RoleService().GetAll();
                Role role = roleList.FirstOrDefault(o => o.Name.Contains("TestRole_"));

                while (role == null)
                {
                    await TestPostMethod();

                    roleList = await new RoleService().GetAll();
                    role = roleList.FirstOrDefault(o => o.Name.Contains("TestRole_"));
                }

                RoleStatus req = new RoleStatus()
                {
                    Id = role.Id,
                    //Status = (int)EntityStatus.Disabled,
                    //Status = (int)EntityStatus.Enabled,
                    Status = (int)EntityStatus.Deprecated,
                };

                var result = await RoleClient.UpdateStatus(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}

﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.Wechat;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
namespace GW.PC.Admin.Test.Admin.Trade
{
    [TestClass]
    public class WeChatTest
    {
        private WeChatClient _client;
        public WeChatClient WeChatClient
        {
            get
            {
                if (_client == null)
                    _client = new WeChatClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
                WeChatQuery req = new WeChatQuery();

                req.MerchantId = 1;
                //req.CustomerUserName = "";
                //req.PaymentCenterOrderNumber = "";
                //req.MerchantOrderNumber = "";
                //req.DepositStatus = Models.DepositStatus.AutoSuccess;
                //req.PaymentChannelId = 1;
                //req.CreateAtStart = DateTime.Now;
                //req.CreateAtEnd = DateTime.Now;

                var result = await WeChatClient.Search(req);

                Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var wechat = await new WeChatDepositService().Add(new WeChatDeposit
            {
                Username = "quickPayTest" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                PaymentPlatform = PaymentPlatform.PC,
                RequestedAmount = 10,
                Status = DepositStatus.Unknown,
                Commission = 10,
                MerchantCallbackStatus = MerchantCallbackStatus.Await,
                MerchantOrderNumber = "CK1812141118571616",
                CreatedAt = DateTime.Now,
                PaymentChannelId = 1,
                MerchantId = 1
            });
            var result = await WeChatClient.FindById(wechat.Id);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineToBankCard;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class OnlineToBankCardController : BaseController
    {
        [Route("onlinetobankcard")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<OnlineToBankCardData>> Get([FromUri]OnlineToBankCardQuery req)
        {
            var onlineToBankCardDeposits = await factory.OnlineToBankCardDepositService.QueryAsync(req.Content, "Merchant", "PaymentChannel");

            var result = onlineToBankCardDeposits.Select(o => new OnlineToBankCardData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                PayeeName = o.PayeeName,
                PayerName = o.PayerName,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        [Route("onlinetobankcard")]
        public async Task<OnlineToBankCardDTO> Get(int id)
        {
            var onlineToBankCard = await factory.OnlineToBankCardDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new OnlineToBankCardDTO()
            {
                Id = onlineToBankCard.Id,
                MerchantName = onlineToBankCard.Merchant.Name,
                CustomerUserName = onlineToBankCard.Username,
                DepositIP = onlineToBankCard.DepositIP,
                Amount = onlineToBankCard.ActualAmount.HasValue ? onlineToBankCard.ActualAmount : onlineToBankCard.RequestedAmount,
                ActualAmount = onlineToBankCard.ActualAmount,
                PayerName = onlineToBankCard.PayerName,
                PayeeName = onlineToBankCard.PayeeName,
                PaymentChannelName = onlineToBankCard.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(onlineToBankCard.Status),
                Commission = onlineToBankCard.Commission,
                DepositAddress = onlineToBankCard.DepositAddress,
                CreateAt = onlineToBankCard.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = onlineToBankCard.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = onlineToBankCard.MerchantOrderNumber,
                PaymentCenterOrderNumber = onlineToBankCard.PaymentCenterOrderNumber,
                PaymentOrderNumber = onlineToBankCard.PaymentOrderNumber,
                ManualApprovedAt = onlineToBankCard.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = onlineToBankCard.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
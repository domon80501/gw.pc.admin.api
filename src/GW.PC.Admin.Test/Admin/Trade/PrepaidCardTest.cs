﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.Alipay;
using GW.PC.Admin.Context.Models.ApiModels.Trade.PrepaidCard;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Trade
{
    [TestClass]
    public class PrepaidCardTest
    {
        private PrepaidCardClient _client;
        public PrepaidCardClient AlipayClient
        {
            get
            {
                if (_client == null)
                    _client = new PrepaidCardClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                PrepaidCardQuery req = new PrepaidCardQuery();

                req.MerchantId = 1;
                req.CustomerUserName = "";
                req.PaymentCenterOrderNumber = "";
                req.MerchantOrderNumber = "";
                req.DepositStatus = Models.DepositStatus.AutoSuccess;
                req.PaymentChannel = 1;
                req.CreateAtStart = DateTime.Now;
                req.CreateAtEnd = DateTime.Now;

                var result = await AlipayClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var prepaidCard = await new PrepaidCardDepositService().Add(new PrepaidCardDeposit
            {
                Username = "prepaidCardTest" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                PaymentPlatform = PaymentPlatform.PC,
                RequestedAmount = 10,
                Status = DepositStatus.Unknown,
                Commission = 10,
                MerchantCallbackStatus = MerchantCallbackStatus.Await,
                MerchantOrderNumber = "CK1812141118571616",
                CreatedAt = DateTime.Now,
                PaymentChannelId = 1,
                MerchantId = 1,
                PaymentProviderBankId = 1
            });

            var result = await AlipayClient.FindById(prepaidCard.Id);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

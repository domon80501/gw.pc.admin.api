﻿using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.Merchants.Merchant
{
    public class MerchantQuery : AllowedMerchant
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }
        [JsonProperty("status")]
        public EntityStatus? Status { get; set; }

        public Expression<Func<PC.Models.Merchant, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.Merchant>(true);

                if (!string.IsNullOrEmpty(UserName))
                    query = query.And(x => x.Username.Contains(UserName));

                if (Status.HasValue)
                    query = query.And(x => x.Status == Status);

                if (AllowedMerchantIds.Count > 0)
                    query = query.And(x => AllowedMerchantIds.Contains(x.Id));

                return query;
            }
        }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GW.PC.Admin.Context.Models.ApiModels.System.Role
{
    public class RoleCreate
    {
        [JsonProperty("roleName")]
        public string RoleName { get; set; }
        [JsonProperty("permissionIds")]
        public List<int> PermissionIds { get; set; }
    }
}

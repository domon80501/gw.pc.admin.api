﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.PrepaidCard;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class PrepaidCardController : BaseController
    {
        [Route("prepaidcard")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<PrepaidCardData>> Get([FromUri]PrepaidCardQuery req)
        {
            var prepaidCardDeposits = await factory.PrepaidCardDepositService.QueryAsync(req.Content, "Merchant", "PaymentChannel");

            var result = prepaidCardDeposits.Select(o => new PrepaidCardData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        [Route("prepaidcard")]
        public async Task<PrepaidCardDTO> Get(int id)
        {
            var prepaidCard = await factory.PrepaidCardDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new PrepaidCardDTO()
            {
                Id = prepaidCard.Id,
                MerchantName = prepaidCard.Merchant.Name,
                CustomerUserName = prepaidCard.Username,
                DepositIP = prepaidCard.DepositIP,
                Amount = prepaidCard.ActualAmount.HasValue ? prepaidCard.ActualAmount : prepaidCard.RequestedAmount,
                PaymentChannelName = prepaidCard.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(prepaidCard.Status),
                Commission = prepaidCard.Commission,
                DepositAddress = prepaidCard.DepositAddress,
                CreateAt = prepaidCard.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = prepaidCard.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = prepaidCard.MerchantOrderNumber,
                SystemOrderNumber = prepaidCard.SystemOrderNumber,
                PaymentCenterOrderNumber = prepaidCard.PaymentCenterOrderNumber,
                PaymentOrderNumber = prepaidCard.PaymentOrderNumber,
                ManualApprovedAt = prepaidCard.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = prepaidCard.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
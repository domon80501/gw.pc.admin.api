﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.MerchantWithdrawal;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiWithdraw)]
    public class MerchantWithdrawalController : BaseController
    {
        [Route("merchantWithdrawal")]
        [CustomAuthorization(Permission = PermissionCode.Order_MerchantWithdrawal_Access)]
        public async Task<IEnumerable<MerchantWithdrawalData>> Get([FromUri]MerchantWithdrawalQuery req)
        {
            var merchantWithdrawals = await factory.MerchantWithdrawalService.QueryAsync(req.Content, "Merchant", "PaidBy", "PayeeBank", "PaymentChannel");

            var result = merchantWithdrawals.Select(o => new MerchantWithdrawalData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Username,
                CustomerUserName = o.PaidBy?.Username,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PayeeName = o.PayeeName,
                Amount = o.Amount.ToString("F2"),
                Commission = o.Commission.ToString("F2"),
                BankName = o.PayeeBank.Name,
                PayeeCardNumber = o.PayeeCardNumber,
                CreatedAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                AuditedAt = o.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                WithdrawalStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        [Route("merchantWithdrawal")]
        public async Task<MerchantWithdrawalDTO> Get([FromUri]int id)
        {
            var merchantWithdrawal = await factory.MerchantWithdrawalService.Find(id, "Merchant", "PaidBy", "PayeeBank", "AuditedBy", "PaidBy");

            var result = new MerchantWithdrawalDTO()
            {
                Id = merchantWithdrawal.Id,
                MerchantName = merchantWithdrawal.Merchant.Name,
                CustomerUserName = merchantWithdrawal.PaidBy?.Username,
                PayeeName = merchantWithdrawal.PayeeName,
                Amount = merchantWithdrawal.Amount.ToString("F2"),
                Commission = merchantWithdrawal.Commission.ToString("F2"),
                WithdrawalStatus = Core.Extensions.GetDisplayName(merchantWithdrawal.Status),
                PayeeCardNumber = merchantWithdrawal.PayeeCardNumber,
                BankName = merchantWithdrawal.PayeeBank.Name,
                WithdrawalIP = merchantWithdrawal.WithdrawalIP,
                WithdrawalAddress = merchantWithdrawal.WithdrawalAddress,
                AuditedById = merchantWithdrawal.AuditedBy?.Username,
                AuditedAt = merchantWithdrawal.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaidById = merchantWithdrawal.PaidBy?.Username,
                PaidAt = merchantWithdrawal.PaidAt?.ToString("yyyy/MM/dd HH:mm:ss"),
            };

            return await Task.FromResult(result);
        }
    }
}
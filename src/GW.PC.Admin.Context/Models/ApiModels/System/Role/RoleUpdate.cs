﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GW.PC.Admin.Context.Models.ApiModels.System.Role
{
    public class RoleUpdate
    {
        public RoleUpdate()
        {
            PermissionIds = new List<int>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("roleName")]
        public string RoleName { get; set; }
        [JsonProperty("permissionIds")]
        public List<int> PermissionIds { get; set; }
    }
}

﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.Internal;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Trade
{
    public class InternalTest
    {
        private InternalClient _client;
        public InternalClient InternalClient
        {
            get
            {
                if (_client == null)
                    _client = new InternalClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            InternalQuery req = new InternalQuery();

            req.MerchantId = 1;
            req.CustomerUserName = "";
            req.PaymentCenterOrderNumber = "";
            req.MerchantOrderNumber = "";
            req.DepositStatus = Models.DepositStatus.AutoSuccess;
            req.PaymentChannel = 1;
            req.CreateAtStart = DateTime.Now;
            req.CreateAtEnd = DateTime.Now;

            var result = await InternalClient.Search(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var interalDeposit = await new InternalDepositService().Add(new InternalDeposit
            {
                Username = "interalTest" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                PaymentPlatform = PaymentPlatform.PC,
                RequestedAmount = 10,
                Status = DepositStatus.Unknown,
                Commission = 10,
                MerchantCallbackStatus = MerchantCallbackStatus.Await,
                CreatedAt = DateTime.Now,
                PaymentChannelId = 1,
                MerchantId = 1,
            });

            var result = await InternalClient.FindById(interalDeposit.Id);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

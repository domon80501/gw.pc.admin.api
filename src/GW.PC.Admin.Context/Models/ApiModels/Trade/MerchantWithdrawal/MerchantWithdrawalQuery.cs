﻿using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade.MerchantWithdrawal
{
    public class MerchantWithdrawalQuery
    {
        [JsonProperty("merchantId")]
        public int? MerchantId { get; set; }
        [JsonProperty("payeeUserName")]
        public string PayeeUserName { get; set; }
        [JsonProperty("payeeName")]
        public string PayeeName { get; set; }
        [JsonProperty("payeeCardNumber")]
        public string PayeeCardNumber { get; set; }
        [JsonProperty("paymentChannel")]
        public int? PaymentChannel { get; set; }
        [JsonProperty("merchantOrderNumber")]
        public string MerchantOrderNumber { get; set; }
        [JsonProperty("providerBank")]
        public int? ProviderBank { get; set; }
        [JsonProperty("depositStatus")]
        public int? DepositStatus { get; set; }
        [JsonProperty("createAtStart")]
        public DateTime? CreateAtStart { get; set; }
        [JsonProperty("createAtEnd")]
        public DateTime? CreateAtEnd { get; set; }

        public Expression<Func<PC.Models.MerchantWithdrawal, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.MerchantWithdrawal>(true);

                if (MerchantId.HasValue)
                    query = query.And(o => o.MerchantId == MerchantId.Value);

                if (!string.IsNullOrEmpty(PayeeUserName))
                    query = query.And(o => o.PaidBy.Username.Contains(PayeeUserName));

                if (!string.IsNullOrEmpty(PayeeName))
                    query = query.And(o => o.PayeeName.Contains(PayeeName));

                if (!string.IsNullOrEmpty(PayeeCardNumber))
                    query = query.And(o => o.PayeeCardNumber.Contains(PayeeCardNumber));

                if (PaymentChannel.HasValue)
                    query = query.And(o => o.PaymentChannelId == PaymentChannel.Value);
                
                if (!string.IsNullOrEmpty(MerchantOrderNumber))
                    query = query.And(o => o.MerchantOrderNumber.Contains(MerchantOrderNumber));

                if (ProviderBank.HasValue)
                    query = query.And(o => o.PayeeBankId == ProviderBank.Value);

                if (DepositStatus.HasValue)
                    query = query.And(o => o.Status == (WithdrawalStatus)DepositStatus.Value);

                if (CreateAtStart.HasValue)
                    query = query.And(o => o.CreatedAt >= CreateAtStart.Value);

                if (CreateAtEnd.HasValue)
                    query = query.And(o => o.CreatedAt <= CreateAtEnd.Value);

                return query;
            }
        }
    }
}

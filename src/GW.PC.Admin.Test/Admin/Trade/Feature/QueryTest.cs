﻿using GW.PC.Admin.Api.Client.Trade.Feature;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using GW.PC.UnitTest.Admin.Trade;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Trade.Feature
{
    [TestClass]
    public class QueryTest
    {
        private QueryClient _client;
        public QueryClient QueryClient
        {
            get
            {
                if (_client == null)
                    _client = new QueryClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                var alipayList = await new AlipayDepositService().GetAll();
                AlipayDeposit alipay = alipayList.FirstOrDefault(o => o.Username.Contains("TestAlipay"));

                while (alipay == null)
                {
                    await new AlipayTest().InsertAlipayAsync();

                    alipayList = await new AlipayDepositService().GetAll();
                    alipay = alipayList.FirstOrDefault(o => o.Username.Contains("TestAlipay"));
                }

                var result = await QueryClient.QueryDeposit(alipay.Id, (int)PaymentMethod.Alipay);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}

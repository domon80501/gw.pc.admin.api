﻿using GW.PC.Admin.Api.Client.Admin;
using GW.PC.Admin.Context.Models.ApiModels.Account;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Account
{
    [TestClass]
    public class LoginTest
    {
        private LoginClient _client;
        public LoginClient LoginClient
        {
            get
            {
                if (_client == null)
                    _client = new LoginClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestPostMethod()
        {
            LoginDTO req = new LoginDTO()
            {
                UserName = "admin",
                Password = "admin123"
            };

            var result = await LoginClient.DoLogin(req);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

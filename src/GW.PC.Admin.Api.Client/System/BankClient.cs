﻿using GW.PC.Admin.Context.Models.ApiModels.System.Bank;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.System
{
    public class BankClient : AdminApiClients
    {
        public BankClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystem;
            uri = "bank";
        }

        public async Task<ApiResult<List<BankData>>> Search(BankQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<BankData>>(uri);
        }

        public async Task<ApiResult<BankDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<BankDTO>(uri);
        }

        public async Task<ApiResult> Create(BankDTO req)
        {
            return await PostApiResultAsync(uri, req);
        }

        public async Task<ApiResult> Update(BankDTO req)
        {
            return await PutApiResultAsync(uri, req);
        }

    }
}

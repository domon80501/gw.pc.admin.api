﻿using GW.PC.Core;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http.Filters;

namespace GW.PC.Admin.Api.Attribute
{
    public class ApiExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var baseException = actionExecutedContext.Exception.GetBaseException();
            string errorCode = Constants.PCAdminMessages.ExceptionCode;
            string message = baseException.Message;

            if (baseException is BusinessException be)
            {
                errorCode = be.ErrorCode;
                message = be.Message;
            }
            else
            {
                message = Constants.PCAdminMessages.InternalError;
                SerilogLogger.Logger.LogError(baseException.ToString());
            }

            var errors = new List<string>();
            errors.Add(baseException.Message);

            var result = new ApiResult
            {
                ResultCode = errorCode,
                IsBusinessError = actionExecutedContext.Exception is BusinessException,
                ResultMessage = message,
                Errors = errors
            };
        
            SerilogLogger.Logger.LogInformation($"Response sent at {DateTime.Now.ToE8().ToDateTimeString()}: {JsonConvert.SerializeObject(result)}");

            var responseMessage = actionExecutedContext.Request.CreateResponse(
                HttpStatusCode.InternalServerError,
                result,
                JsonMediaTypeFormatter.DefaultMediaType);
            
            responseMessage.Headers.Add("Access-Control-Allow-Origin", "*");
            responseMessage.Headers.Add("Access-Control-Allow-Methods", "*");// "POST,GET,OPTIONS,PUT,PATCH");
            responseMessage.Headers.Add("Access-Control-Allow-Headers", "*");// "Content-Type");

            actionExecutedContext.Response = responseMessage;
        }
    }
}

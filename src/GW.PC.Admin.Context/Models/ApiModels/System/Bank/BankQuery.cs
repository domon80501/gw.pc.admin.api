﻿using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.System.Bank
{
    public class BankQuery
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        public Expression<Func<PC.Models.Bank, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.Bank>(true);

                if (!string.IsNullOrEmpty(Name))
                    query = query.And(x => x.Name.Contains(Name));

                return query;
            }
        }
    }
}

﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Account
{
    public class LoginDTO
    {
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}

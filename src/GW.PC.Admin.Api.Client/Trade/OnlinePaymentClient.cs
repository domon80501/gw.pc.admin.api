﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlinePayment;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class OnlinePaymentClient : AdminApiClients
    {
        public OnlinePaymentClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiTrade;
            uri = "onlinepaymennt";
        }

        public async Task<ApiResult<List<OnlinePaymentData>>> Search(OnlinePaymentQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<OnlinePaymentData>>(uri);
        }

        public async Task<ApiResult<OnlinePaymentDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<OnlinePaymentDTO>(uri);
        }
    }
}

﻿using GW.PC.Admin.Context.Models.ApiModels.System.EntityLog;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.System
{
    public class EntityLogClient : AdminApiClients
    {
        public EntityLogClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystem;
            uri = "entityLog";
        }

        public async Task<ApiResult<List<EntityLogData>>> Search(EntityLogQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<EntityLogData>>(uri);
        }

        public async Task<ApiResult<EntityLogData>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<EntityLogData>(uri);
        }
    }
}

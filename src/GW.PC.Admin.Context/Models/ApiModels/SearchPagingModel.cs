﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels
{
    public class SearchPagingModel
    {
        public SearchPagingModel()
        {
            PageNumber = "1";
            PageSize = "10";
            ColName = string.Empty;
            Descending = false;
        }
        
        [JsonProperty("pageNum")]
        public string PageNumber { get; set; }
        [JsonProperty("pageSize")]
        public string PageSize { get; set; }
        [JsonProperty("colName")]
        public string ColName { get; set; }
        [JsonProperty("descending")]
        public bool Descending { get; set; }
    }
}

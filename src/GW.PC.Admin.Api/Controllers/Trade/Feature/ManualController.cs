﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Models.Infrastructure;
using GW.PC.Models.Queues;
using GW.PC.Web.Core;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade.Feature
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiTradeFeature)]
    public class ManualController : BaseController
    {
        [Route("manual")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Manual)]
        public async Task Get(int id, PaymentMethod paymentMethod)
        {
            Deposit deposit = await GetDepositByIdAndPaymentMethod(id, paymentMethod);

            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            MerchantCallbackRequest merchantQueue = new MerchantCallbackRequest()
            {
                RequestId = Guid.NewGuid().ToString(),
                MerchantCallbackUrl = deposit.RedirectionUrl,
                MerchantUsername = deposit.Merchant.Username,
                PaymentType = PaymentType.Deposit,
                PaymentMethod = paymentMethod,
                MerchantOrderNumber = deposit.MerchantOrderNumber,
                PaymentCenterOrderNumber = deposit.PaymentCenterOrderNumber,
                PaymentOrderNumber = deposit.PaymentOrderNumber,
                RequestedAmount = deposit.RequestedAmount.ToString("F2"),
                ActualAmount = deposit.ActualAmount?.ToString("F2"),
                Status = TransferTransactionStatus(deposit.Status),
                CreatedAt = DateTime.Now.ToE8(),
                NextSendingAt = DateTime.Now.ToE8(),
            };

            await factory.MerchantCallbackRequestService.Add(merchantQueue);
        }

        [CustomAuthorization(Permission = PermissionCode.Order_MerchantWithdrawal_Manual)]
        public async Task Get(int id)
        {
            MerchantWithdrawal withdrawal = await factory.MerchantWithdrawalService.Find(id, "Merchant", "PaymentChannel");

            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            MerchantCallbackRequest merchantQueue = new MerchantCallbackRequest()
            {
                RequestId = Guid.NewGuid().ToString(),
                MerchantCallbackUrl = withdrawal.CallbackUrl,
                MerchantUsername = withdrawal.Merchant.Username,
                PaymentType = PaymentType.Withdrawal,
                PaymentMethod = withdrawal.PaymentChannel.PaymentMethod,
                MerchantOrderNumber = withdrawal.MerchantOrderNumber,
                PaymentCenterOrderNumber = withdrawal.PaymentCenterOrderNumber,
                PaymentOrderNumber = withdrawal.PaymentOrderNumber,
                RequestedAmount = withdrawal.Amount.ToString("F2"),
                ActualAmount = withdrawal.Amount.ToString("F2"),
                Status = TransferTransactionStatus(withdrawal.Status),
                CreatedAt = DateTime.Now.ToE8(),
                NextSendingAt = DateTime.Now.ToE8(),
            };

            await factory.MerchantCallbackRequestService.Add(merchantQueue);
        }
    }
}
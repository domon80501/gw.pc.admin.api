﻿using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.System.Role
{
    public class RoleQuery
    {
        [JsonProperty("roleName")]
        public string RoleName { get; set; }
        [JsonProperty("status")]
        public int? Status { get; set; }

        public Expression<Func<PC.Models.Role, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.Role>(true);

                if (!string.IsNullOrEmpty(RoleName))
                    query = query.And(x => x.Name.Contains(RoleName));
                
                if (Status.HasValue)
                    query = query.And(x => x.Status == (EntityStatus)Status);

                return query;
            }
        }
    }
}

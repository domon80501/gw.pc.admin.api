﻿using GW.PC.Core;
using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentProviderBank
{
    public class PaymentProviderBankQuery : AllowedMerchant
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("status")]
        public EntityStatus? Status { get; set; }

        [JsonProperty("paymentMethodId")]
        public PaymentMethod? PaymentMethodId { get; set; }

        [JsonProperty("paymentTypeId")]
        public PaymentType? PaymentTypeId { get; set; }

        public Expression<Func<PC.Models.PaymentProviderBank, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.PaymentProviderBank>(true);

                if (!string.IsNullOrEmpty(Name))
                    query = query.And(x => x.PaymentProvider.Name.Contains(Name));

                if (PaymentMethodId.HasValue)
                    query = query.And(x => x.PaymentMethod == PaymentMethodId.Value);

                if (PaymentTypeId.HasValue)
                    query = query.And(x => x.PaymentType == PaymentTypeId.Value);

                if (Status.HasValue)
                    query = query.And(x => x.Status == Status.Value);

                if (AllowedMerchantIds.Count > 0)
                    query = query.And(o => AllowedMerchantIds.Contains(o.MerchantId));

                return query;
            }
        }
    }
}

﻿using GW.PC.Admin.Context.Models.ApiModels.SystemExtension.PermissionGroup;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.SystemExtension
{
    public class PermissionGroupCliient : AdminApiClients
    {
        public PermissionGroupCliient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystemExtension;
            uri = "permissiongroup";
        }

        public async Task<ApiResult<List<PermissionListData>>> Search()
        {
            return await GetApiResultAsync<List<PermissionListData>>(uri);
        }
    }
}

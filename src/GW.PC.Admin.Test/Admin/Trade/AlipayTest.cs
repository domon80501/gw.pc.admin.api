﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.Alipay;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.UnitTest.Admin.Trade
{
    [TestClass]
    public class AlipayTest
    {
        private AlipayClient _client;
        public AlipayClient AlipayClient
        {
            get
            {
                if (_client == null)
                    _client = new AlipayClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                AlipayQuery req = new AlipayQuery();

                //req.MerchantId = 1;
                //req.CustomerUserName = "";
                //req.PaymentCenterOrderNumber = "";
                //req.MerchantOrderNumber = "";
                //req.DepositStatus = Models.DepositStatus.AutoSuccess;
                //req.PaymentChannelId = 1;
                //req.CreateAtStart = DateTime.Now;
                //req.CreateAtEnd = DateTime.Now;

                var result = await AlipayClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            try
            {
                var alipayList = await new AlipayDepositService().GetAll();
                AlipayDeposit alipay = alipayList.FirstOrDefault(o => o.Username.Contains("TestAlipay"));

                while (alipay == null)
                {
                    await InsertAlipayAsync();

                    alipayList = await new AlipayDepositService().GetAll();
                    alipay = alipayList.FirstOrDefault(o => o.Username.Contains("TestAlipay"));
                }

                int id = alipay.Id;
                var result = await AlipayClient.FindById(id);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task InsertAlipayAsync()
        {
            AlipayDeposit alipay = new AlipayDeposit();

            alipay.Username = "TestAlipay" + RandomGenerator.RandomString(10);
            alipay.PaymentPlatform = PaymentPlatform.All;
            alipay.RequestedAmount = 100;
            alipay.ActualAmount = 50;
            alipay.MerchantOrderNumber = "12345678";
            alipay.PaymentCenterOrderNumber = "2345678";
            alipay.PaymentOrderNumber = "54321";
            alipay.DepositIP = "127.0.0.1";
            alipay.DepositAddress = "address";
            alipay.CompletedAt = DateTime.Now;
            alipay.Status = DepositStatus.AutoSuccess;
            alipay.CallbackRawText = null;
            alipay.Commission = 0;
            alipay.RequestRawText = null;
            alipay.PaymentChannelId = 1;
            alipay.MerchantId = 2;
            alipay.PromotionId = 1;
            alipay.SystemOrderNumber = "systemordernumber";
            alipay.CallbackUrl = "callbackUrl";
            alipay.RowVersion = null;
            alipay.RedirectionUrl = "redirectionurl";
            alipay.MerchantCallbackStatus = MerchantCallbackStatus.Await;

            await new AlipayDepositService().Add(alipay);
        }

       
    }
}

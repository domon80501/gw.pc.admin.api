﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentChannel
{
    public class SuccessRateDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("successRates")]
        public string SuccessRates { get; set; }
    }
}

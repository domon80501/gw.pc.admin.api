﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.System;
using GW.PC.Admin.Context.Models.ApiModels.System.User;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF.Admin;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GW.PC.UnitTest.Admin.System
{
    [TestClass]
    public class UserTest
    {
        private UserClient _client;
        public UserClient UserClient
        {
            get
            {
                if (_client == null)
                    _client = new UserClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestPostMethod()
        {
            try
            {
                int[] roleIds = new[] { 1, 2, 3, 4 };

                var userName = "TestUser_" + RandomGenerator.RandomString(10);

                bool isExist = await IsExistUser(userName);
                while (isExist)
                {
                    userName = "TestUser_" + RandomGenerator.RandomString(10);
                    isExist = await IsExistUser(userName);
                }

                UserCreate req = new UserCreate()
                {
                    UserName = userName,
                    Password = "testPassword33",
                    RoleId = roleIds,
                };

                var result = await UserClient.Create(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private async Task<bool> IsExistUser(string userName)
        {
            var userList = await new UserService().GetAll();
            var user = userList.FirstOrDefault(o => o.Username == userName);

            return user != null;
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                UserQuery req = new UserQuery();

                var result = await UserClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            try
            {
                var userList = await new UserService().GetAll();
                User user = userList.FirstOrDefault(o => o.Username.Contains("TestUser"));

                while (user == null)
                {
                    await TestPostMethod();

                    userList = await new UserService().GetAll();
                    user = userList.FirstOrDefault(o => o.Username.Contains("TestUser"));
                }

                int id = user.Id;
                var result = await UserClient.FindById(id);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        [TestMethod]
        public async Task TestPutMethod()
        {
            try
            {
                var userList = await new UserService().GetAll();
                User user = userList.FirstOrDefault(o => o.Username.Contains("TestUser"));

                while (user == null)
                {
                    await TestPostMethod();

                    userList = await new UserService().GetAll();
                    user = userList.FirstOrDefault(o => o.Username.Contains("TestUser"));
                }

                UserUpdate req = new UserUpdate()
                {
                    Id = user.Id,
                    RoleIds = new[] { 3 },
                };

                var result = await UserClient.Update(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestPatchMethod()
        {
            try
            {
                var userList = await new UserService().GetAll();
                User user = userList.FirstOrDefault(o => o.Username.Contains("TestUser"));

                while (user == null)
                {
                    await TestPostMethod();

                    userList = await new UserService().GetAll();
                    user = userList.FirstOrDefault(o => o.Username.Contains("TestUser"));
                }

                UserStatus req = new UserStatus()
                {
                    Id = user.Id,
                    //Status = (int)EntityStatus.Disabled,
                    //Status = (int)EntityStatus.Enabled,
                    Status = (int)EntityStatus.Deprecated,
                };

                var result = await UserClient.UpdateStatus(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}

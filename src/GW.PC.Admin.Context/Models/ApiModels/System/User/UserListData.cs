﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GW.PC.Admin.Context.Models.ApiModels.System.User
{
    public class UserListData
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("userName")]
        public string UserName { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("createdByUserName")]
        public string CreatedByUserName { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("userRoles")]
        public List<int> UserRoles { get; set; }
    }
}

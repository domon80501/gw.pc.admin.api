﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.EntityLog
{
    public class EntityLogData
    {
        [JsonProperty("targetType")]
        public string TargetType { get; set; }
        [JsonProperty("targetId")]
        public string TargetId { get; set; }
        [JsonProperty("content")]
        public string Content { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("details")]
        public string Details { get; set; }
    }
}

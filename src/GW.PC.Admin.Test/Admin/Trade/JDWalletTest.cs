﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.JDWallet;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Trade
{
    [TestClass]
    public class JDWalletTest
    {
        private JDWalletClient _client;
        public JDWalletClient JDWalletClient
        {
            get
            {
                if (_client == null)
                    _client = new JDWalletClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            JDWalletQuery req = new JDWalletQuery();

            req.MerchantId = 1;
            req.CustomerUserName = "";
            req.PaymentCenterOrderNumber = "";
            req.MerchantOrderNumber = "";
            req.DepositStatus = Models.DepositStatus.AutoSuccess;
            req.PaymentChannel = 1;
            req.CreateAtStart = DateTime.Now;
            req.CreateAtEnd = DateTime.Now;

            var result = await JDWalletClient.Search(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var jdWallet = await new JDWalletDepositService().Add(new JDWalletDeposit
            {
                Username = "jdWalletTest" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                MerchantOrderNumber = "CK1812141118571616",
                PaymentPlatform = PaymentPlatform.PC,
                RequestedAmount = 10,
                Status = DepositStatus.Unknown,
                Commission = 10,
                MerchantCallbackStatus = MerchantCallbackStatus.Await,
                CreatedAt = DateTime.Now,
                PaymentChannelId = 1,
                MerchantId = 1,
            });
            var result = await JDWalletClient.FindById(jdWallet.Id);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

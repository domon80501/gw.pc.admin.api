﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.Internal;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class InternalController : BaseController
    {
        [Route("internal")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<InternalData>> Get([FromUri]InternalQuery req)
        {
            var internalDeposits = await factory.InternalDepositService.QueryAsync(req.Content, "Merchant", "PaymentChannel");

            var result = internalDeposits.Select(o => new InternalData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        [Route("internal")]
        public async Task<InternalDTO> Get(int id)
        {
            var internalDeposit = await factory.InternalDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new InternalDTO()
            {
                Id = internalDeposit.Id,
                MerchantName = internalDeposit.Merchant.Name,
                CustomerUserName = internalDeposit.Username,
                DepositIP = internalDeposit.DepositIP,
                Amount = internalDeposit.ActualAmount.HasValue ? internalDeposit.ActualAmount : internalDeposit.RequestedAmount,
                PaymentChannelName = internalDeposit.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(internalDeposit.Status),
                Commission = internalDeposit.Commission,
                DepositAddress = internalDeposit.DepositAddress,
                CreateAt = internalDeposit.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = internalDeposit.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = internalDeposit.MerchantOrderNumber,
                SystemOrderNumber = internalDeposit.SystemOrderNumber,
                PaymentCenterOrderNumber = internalDeposit.PaymentCenterOrderNumber,
                PaymentOrderNumber = internalDeposit.PaymentOrderNumber,
                ManualApprovedAt = internalDeposit.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = internalDeposit.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
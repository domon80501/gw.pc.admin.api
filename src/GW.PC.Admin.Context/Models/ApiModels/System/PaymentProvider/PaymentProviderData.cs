﻿using GW.PC.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentProvider
{
    public class PaymentProviderData
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("createdById")]
        public string CreatedById { get; set; }
        [JsonProperty("status")]
        public EntityStatus Status { get; set; }
    }
}

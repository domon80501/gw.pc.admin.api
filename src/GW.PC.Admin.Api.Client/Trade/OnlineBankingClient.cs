﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineBanking;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace GW.PC.Admin.Api.Client.Trade
{
    public class OnlineBankingClient : AdminApiClients
    {
        public OnlineBankingClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "onlineBanking";
        }

        public async Task<ApiResult<List<OnlineBankingData>>> Search(OnlineBankingQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<OnlineBankingData>>(uri);
        }

        public async Task<ApiResult<OnlineBankingDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<OnlineBankingDTO>(uri);
        }
    }
}

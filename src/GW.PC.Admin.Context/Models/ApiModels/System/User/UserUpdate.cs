﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.User
{
    public class UserUpdate
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("roleId")]
        public int[] RoleId { get; set; }
    }
}

﻿using GW.PC.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Admin.Context.Models
{
    public class Feature
    {
        public Feature()
        {
            IsEnableEdit = true;
        }

        public Feature(DepositStatus status)
        {
            IsEnableDetail = true;
            IsEnableQuery = status == DepositStatus.PendingPayment;
            IsEnableManual =
                status != DepositStatus.PendingPayment &&
                status != DepositStatus.AwaitReminderApproval;
        }

        public Feature(WithdrawalStatus status)
        {
            IsEnableDetail = true;
            IsEnableQuery = status == WithdrawalStatus.AutoPaymentInProgress;
            IsEnableManual =
                status == WithdrawalStatus.AutoPaymentSuccessful
                || status == WithdrawalStatus.AutoPaymentFailed
                || status == WithdrawalStatus.ManualConfirmed
                || status == WithdrawalStatus.FailedAndClosed
                || status == WithdrawalStatus.RetryAutoPaymentSuccessful
                || status == WithdrawalStatus.RetryManualConfirmed;
        }

        [JsonProperty("isEnableDetail")]
        public bool IsEnableDetail { get; set; }
        [JsonProperty("isEnableEdit")]
        public bool IsEnableEdit { get; set; }
        [JsonProperty("isEnableQuery")]
        public bool IsEnableQuery { get; set; }
        [JsonProperty("isEnableManual")]
        public bool IsEnableManual { get; set; }
    }
}

﻿using GW.PC.Admin.Context.Models.ApiModels.System.PaymentProvider;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.System
{
    public class PaymentProviderClient : AdminApiClients
    {
        public PaymentProviderClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystem;
            uri = "paymentProvider";
        }

        public async Task<ApiResult<List<PaymentProviderData>>> Search(PaymentProviderQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<PaymentProviderData>>(uri);
        }

        public async Task<ApiResult<PaymentProviderDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<PaymentProviderDTO>(uri);
        }

        public async Task<ApiResult> Create(PaymentProviderDTO req)
        {
            return await PostApiResultAsync(uri, req);
        }

        public async Task<ApiResult> Update(PaymentProviderDTO req)
        {
            return await PutApiResultAsync(uri, req);
        }

        public async Task<ApiResult> UpdateStatus(PaymentProviderStatus req)
        {
            return await PatchApiResultAsync(uri, req);
        }
    }
}

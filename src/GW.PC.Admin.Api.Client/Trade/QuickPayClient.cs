﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.QuickPay;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class QuickPayClient : AdminApiClients
    {
        public QuickPayClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "quickpay";
        }

        public async Task<ApiResult<List<QuickPayData>>> Search(QuickPayQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<QuickPayData>>(uri);
        }

        public async Task<ApiResult<QuickPayDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<QuickPayDTO>(uri);
        }
    }
}

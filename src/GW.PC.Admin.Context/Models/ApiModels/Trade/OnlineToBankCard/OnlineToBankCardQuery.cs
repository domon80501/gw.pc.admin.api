﻿using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineToBankCard
{
    public class OnlineToBankCardQuery : AllowedMerchant
    {
        [JsonProperty("merchantId")]
        public int? MerchantId { get; set; }
        [JsonProperty("customerUserName")]
        public string CustomerUserName { get; set; }
        [JsonProperty("payerName")]
        public string PayerName { get; set; }
        [JsonProperty("payeeName")]
        public string PayeeName { get; set; }
        [JsonProperty("merchantOrderNumber")]
        public string MerchantOrderNumber { get; set; }
        [JsonProperty("paymentCenterOrderNumber")]
        public string PaymentCenterOrderNumber { get; set; }
        [JsonProperty("depositStatus")]
        public DepositStatus? DepositStatus { get; set; }
        [JsonProperty("createAtStart")]
        public DateTime? CreateAtStart { get; set; }
        [JsonProperty("createAtEnd")]
        public DateTime? CreateAtEnd { get; set; }

        public Expression<Func<OnlineToBankCardDeposit, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<OnlineToBankCardDeposit>(true);

                if (MerchantId.HasValue)
                    query = query.And(o => o.MerchantId == MerchantId.Value);

                if (!string.IsNullOrEmpty(CustomerUserName))
                    query = query.And(o => o.Username.Contains(CustomerUserName));

                if (!string.IsNullOrEmpty(PayeeName))
                    query = query.And(o => o.PayeeName.Contains(MerchantOrderNumber));

                if (!string.IsNullOrEmpty(PayerName))
                    query = query.And(o => o.PayeeName.Contains(MerchantOrderNumber));

                if (!string.IsNullOrEmpty(PaymentCenterOrderNumber))
                    query = query.And(o => o.PaymentCenterOrderNumber.Contains(PaymentCenterOrderNumber));

                if (!string.IsNullOrEmpty(MerchantOrderNumber))
                    query = query.And(o => o.MerchantOrderNumber.Contains(MerchantOrderNumber));

                if (DepositStatus.HasValue)
                    query = query.And(o => o.Status == DepositStatus.Value);

                if (CreateAtStart.HasValue)
                    query = query.And(o => o.CreatedAt >= CreateAtStart.Value);

                if (CreateAtEnd.HasValue)
                    query = query.And(o => o.CreatedAt <= CreateAtEnd.Value);

                if (AllowedMerchantIds.Count > 0)
                    query = query.And(o => AllowedMerchantIds.Contains(o.MerchantId.Value));

                return query;
            }
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Admin.Context.Enum
{
    public enum DateType
    {
        [Display(Name = "日")]
        Day = 1,
        [Display(Name = "月")]
        Month
    }
}

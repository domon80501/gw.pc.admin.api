﻿using System.Collections.Generic;

namespace GW.PC.Admin.Context.Models.ApiModels
{
    public class AllowedMerchant : SearchPagingModel
    {
        public AllowedMerchant()
        {
            AllowedMerchantIds = new List<int>();
        }

        public List<int> AllowedMerchantIds { get; set; }
    }
}

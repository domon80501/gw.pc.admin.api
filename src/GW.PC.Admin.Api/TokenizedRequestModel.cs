﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Api
{
    public class TokenizedRequestModel //: MainWapPortalRequestModel
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        public string Username { get; set; }
    }
}

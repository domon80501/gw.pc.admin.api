﻿using GW.PC.Admin.Context.Enum;
using GW.PC.Core;
using Newtonsoft.Json;
using System;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentChannel
{
    public class PaymentChannelSuccessRateQuery
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("successRateDateType")]
        public int? SuccessRateDateType { get; set; }
        [JsonProperty("date")]
        public string Date { get; set; }
        [JsonProperty("month")]
        public string Month { get; set; }

        public DateTime GetPeriodStartsAt()
        {
            var today = DateTime.Today;

            if (!SuccessRateDateType.HasValue)
            {
                return today;
            }

            if (SuccessRateDateType == (int)DateType.Day)
            {
                if (!string.IsNullOrEmpty(Date))
                {
                    return DateTime.ParseExact(Date, Constants.FormatStrings.DateFormat, null);
                }
                else
                {
                    return today;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Month))
                {
                    return DateTime.ParseExact(Month, Constants.FormatStrings.MonthFormat, null);
                }
                else
                {

                    return new DateTime(today.Year, today.Month, 1);
                }
            }
        }

        public DateTime GetPeriodEndsAt()
        {
            var today = DateTime.Now;

            if (!SuccessRateDateType.HasValue)
            {
                return today.AddDays(1);
            }

            if (SuccessRateDateType == (int)DateType.Day)
            {
                return GetPeriodStartsAt().AddDays(1);
            }
            else
            {
                return GetPeriodStartsAt().AddMonths(1);
            }
        }
    }
}

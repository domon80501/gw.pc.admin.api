﻿using System;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.System;
using GW.PC.Admin.Context.Models.ApiModels.System.Bank;
using GW.PC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GW.PC.Admin.Test.Admin.System
{
    [TestClass]
    public class BankTest
    {
        private BankClient _client;
        public BankClient BankClient
        {
            get
            {
                if (_client == null)
                    _client = new BankClient();

                return _client;
            }
        }


        [TestMethod]
        public async Task TestGetMethod()
        {
            BankQuery req = new BankQuery();

            req.Name = "ICBC";

            var result = await BankClient.Search(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            int id = 1;
            var result = await BankClient.FindById(id);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestPostMethod()
        {
            BankDTO req = new BankDTO()
            {
                Name = "BankTest" + new Random().Next(1,999),
                Code = "00" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                Url = "127.0.0.1",
                CreatedAt = DateTime.Now.ToString(),
                CreatedById = "1",
            };

            var result = await BankClient.Create(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestPutMethod()
        {
            BankDTO req = new BankDTO()
            {
                Id = 3,
                Name = "BankTest3",
                Code = "002",
                Url = "127.0.0.1",
                CreatedAt = DateTime.Now.ToString(),
                CreatedById = "1"
            };

            var result = await BankClient.Update(req);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

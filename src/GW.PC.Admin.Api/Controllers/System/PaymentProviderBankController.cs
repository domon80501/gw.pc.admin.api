﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.System.PaymentProviderBank;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.System
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystem)]
    public class PaymentProviderBankController : BaseController
    {
        [Route("paymentProviderBank")]
        public void Options()
        { }

        [Route("paymentProviderBank")]
        [CustomAuthorization(Permission = PermissionCode.System_PaymentProviderBank_Access)]
        public async Task<IEnumerable<PaymentProviderBankData>> Get([FromUri]PaymentProviderBankQuery req)
        {

            var paymentProviderBanks = await factory.PaymentProviderBankService.QueryAsync(req.Content,"Bank", "PaymentProvider");
            return paymentProviderBanks.Select(s => new PaymentProviderBankData
            {
                Id = s.Id,
                PaymentProviderBankName = s.Bank.Name,
                PaymentProviderName = s.PaymentProvider.Name,
                PaymentTypeName = Extensions.GetDisplayName(s.PaymentType),
                PaymentMethodName = Extensions.GetDisplayName(s.PaymentMethod),
                PaymentProviderBankCode = s.Code,
                Order = s.Order,
                Status = s.Status
            }).ToList();
        }

        [Route("paymentProviderBank")]
        public async Task<PaymentProviderBankDTO> Get(int id)
        {
            var paymentProviderBank = await factory.PaymentProviderBankService.Find(id);
            return new PaymentProviderBankDTO()
            {
                Id = paymentProviderBank.Id,
                BankId = paymentProviderBank.BankId,
                PaymentProviderId = paymentProviderBank.PaymentProviderId,
                PaymentType = (int)paymentProviderBank.PaymentType,
                PaymentMethod = (int)paymentProviderBank.PaymentMethod,
                PaymentProviderBankCode = paymentProviderBank.Code,
                Order = paymentProviderBank.Order,
                Status = paymentProviderBank.Status
            };
        }

        [Route("paymentProviderBank")]
        [CustomAuthorization(Permission = PermissionCode.System_PaymentProviderBank_Create)]
        public async Task Post([FromBody]PaymentProviderBankDTO req)
        {
            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            PaymentProviderBank paymentProviderBank = new PaymentProviderBank
            {
                Code = req.PaymentProviderBankCode,
                PaymentType = (PaymentType)req.PaymentType,
                PaymentMethod = (PaymentMethod)req.PaymentMethod,
                Status = EntityStatus.Enabled,
                Order = req.Order,
                BankId = req.BankId,
                PaymentProviderId = req.PaymentProviderId,
                MerchantId = req.MerchantId,

                CreatedAt = DateTime.Now.ToE8(),
                CreatedById = createId

            };
            await factory.PaymentProviderBankService.Save(paymentProviderBank);
        }

        [Route("paymentProviderBank")]
        [CustomAuthorization(Permission = PermissionCode.System_PaymentProviderBank_Detail_Edit)]
        public async Task Put([FromBody]PaymentProviderBankDTO req)
        {
            PaymentProviderBank paymentProviderBank = await factory.PaymentProviderBankService.Find(req.Id);
            paymentProviderBank.Code = req.PaymentProviderBankCode;
            paymentProviderBank.PaymentType = (PaymentType)req.PaymentType;
            paymentProviderBank.PaymentMethod = (PaymentMethod)req.PaymentMethod;
            paymentProviderBank.Status = req.Status;
            paymentProviderBank.Order = req.Order;
            paymentProviderBank.BankId = req.BankId;
            paymentProviderBank.PaymentProviderId = req.PaymentProviderId;

            await factory.PaymentProviderBankService.Save(paymentProviderBank);
        }

        [Route("paymentProviderBank")]
        [CustomAuthorization(Permission = PermissionCode.System_PaymentProviderBank_Detail_Status_Edit)]
        public async Task Patch(PaymentProviderBankStatus req)
        {
            PaymentProviderBank paymentProviderBank = await factory.PaymentProviderBankService.Find(req.Id);

            paymentProviderBank.Status = req.Status;

            await factory.PaymentProviderBankService.Save(paymentProviderBank);
        }
    }
}
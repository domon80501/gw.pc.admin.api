﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.QQWallet;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Trade
{
    [TestClass]
    public class QQWalletTest
    {
        private QQWalletClient _client;
        public QQWalletClient QQWalletClient
        {
            get
            {
                if (_client == null)
                    _client = new QQWalletClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            QQWalletQuery req = new QQWalletQuery();

            req.MerchantId = 1;
            req.CustomerUserName = "";
            req.PaymentCenterOrderNumber = "";
            req.MerchantOrderNumber = "";
            req.DepositStatus = Models.DepositStatus.AutoSuccess;
            req.PaymentChannel = 1;
            req.CreateAtStart = DateTime.Now;
            req.CreateAtEnd = DateTime.Now;

            var result = await QQWalletClient.Search(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var qqWallet = await new QQWalletDepositService().Add(new QQWalletDeposit
            {
                Username = "qqWalletTest" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                PaymentPlatform = PaymentPlatform.PC,
                RequestedAmount = 10,
                Status = DepositStatus.Unknown,
                Commission = 10,
                MerchantCallbackStatus = MerchantCallbackStatus.Await,
                MerchantOrderNumber = "CK1812141118571616",
                CreatedAt = DateTime.Now,
                PaymentChannelId = 1,
                MerchantId = 1
            });
            var result = await QQWalletClient.FindById(qqWallet.Id);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

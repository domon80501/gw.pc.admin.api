﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.Wechat;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class WeChatClient : AdminApiClients
    {
        public WeChatClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "wechat";
        }

        public async Task<ApiResult<List<WeChatData>>> Search(WeChatQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<WeChatData>>(uri);
        }

        public async Task<ApiResult<WeChatDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<WeChatDTO>(uri);
        }
    }
}

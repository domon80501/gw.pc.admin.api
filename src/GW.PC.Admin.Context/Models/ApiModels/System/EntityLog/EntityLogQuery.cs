﻿using GW.PC.Models;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace GW.PC.Admin.Context.Models.ApiModels.System.EntityLog
{
    public class EntityLogQuery
    {
        [JsonProperty("targetType")]
        public EntityLogTargetType? TargetType { get; set; }
        [JsonProperty("targetId")]
        public int? TargetId { get; set; }

        public Expression<Func<PC.Models.EntityLog, bool>> Content
        {
            get
            {
                var query = PredicateBuilder.New<PC.Models.EntityLog>(true);

                // 两个参数都不给的话, 不捞值
                if (!TargetType.HasValue && !TargetId.HasValue)
                    query = query.And(o => o.Id == -1);

                if (TargetType.HasValue)
                    query = query.And(o => o.TargetType == TargetType.Value);

                if (TargetId.HasValue)
                    query = query.And(o => o.TargetId == TargetId.Value);

                return query;
            }
        }
    }
}

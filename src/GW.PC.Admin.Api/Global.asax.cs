﻿using System.Web.Http;

namespace GW.PC.Admin.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {            
            GlobalConfiguration.Configure(WebApiConfig.Register);            
        }
    }
}

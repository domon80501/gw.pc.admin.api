﻿using GW.PC.Core;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client
{
    public class AdminApiClients : WebApiClientBase
    {
        protected static HttpClient client = null;
        protected static string uri = string.Empty;

        public AdminApiClients()
        {
            client = client ?? new HttpClient
            {
                BaseAddress = //new Uri("http://localhost:61989/")
                
                new Uri(Constants.AppSettings.Get(
                    AppSettingNames.GWPCAdminWebApiAddress))
            };

            //RoutePrefix = WebApiControllerRoutePrefixes.PaymentGateway;
        }

        protected override HttpClient GetHttpClient()
        {
            return client;
        }

        protected async Task<ApiResult> GetApiResultAsync(string uri)
        {
            var response = await GetHttpClient().GetAsync($"{RoutePrefix}/{uri}");

            return await response.Content?.ReadAsAsync<ApiResult>();
        }

        protected async Task<ApiResult<TResult>> GetApiResultAsync<TResult>(string uri)
        {
            var response = await GetHttpClient().GetAsync($"{RoutePrefix}/{uri}");
            
            return await response.Content?.ReadAsAsync<ApiResult<TResult>>();
        }

        protected async Task<ApiResult> PostApiResultAsync<TModel>(string uri, TModel model)
        {
            var response = await GetHttpClient().PostAsJsonAsync($"{RoutePrefix}/{uri}", model);

            return await response.Content?.ReadAsAsync<ApiResult>();
        }

        protected async Task<ApiResult> PutApiResultAsync<TModel>(string uri, TModel model)
        {
            var response = await GetHttpClient().PutAsJsonAsync($"{RoutePrefix}/{uri}", model);

            return await response.Content?.ReadAsAsync<ApiResult>();
        }

        protected async Task<ApiResult> PatchApiResultAsync<TModel>(string uri, TModel model)
        {
            var myContent = JsonConvert.SerializeObject(model);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await GetHttpClient().SendAsync(new HttpRequestMessage(new HttpMethod("PATCH"), $"{RoutePrefix}/{uri}")
            {
                Content = byteContent
            });

            return await response.Content?.ReadAsAsync<ApiResult>();
        }
    }
}

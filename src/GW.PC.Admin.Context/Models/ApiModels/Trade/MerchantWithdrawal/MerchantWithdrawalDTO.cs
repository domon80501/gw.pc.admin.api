﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade.MerchantWithdrawal
{
    public class MerchantWithdrawalDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("merchantName")]
        public string MerchantName { get; set; }
        [JsonProperty("customerUserName")]
        public string CustomerUserName { get; set; }
        [JsonProperty("payeeName")]
        public string PayeeName { get; set; }
        [JsonProperty("requestedAmount")]
        public string Amount { get; set; }
        [JsonProperty("commission")]
        public string Commission { get; set; }
        [JsonProperty("withdrawalStatus")]
        public string WithdrawalStatus { get; set; }
        [JsonProperty("payeeCardNumber")]
        public string PayeeCardNumber { get; set; }
        [JsonProperty("bankName")]
        public string BankName { get; set; }
        [JsonProperty("withdrawalIP")]
        public string WithdrawalIP { get; set; }
        [JsonProperty("withdrawalAddress")]
        public string WithdrawalAddress { get; set; }
        [JsonProperty("auditedById")]
        public string AuditedById { get; set; }
        [JsonProperty("auditedAt")]
        public string AuditedAt { get; set; }
        [JsonProperty("paidById")]
        public string PaidById { get; set; }
        [JsonProperty("paidAt")]
        public string PaidAt { get; set; }
    }
}

﻿using System.Threading.Tasks;
using System.Web.Http;
using GW.PC.Admin.Api.Utiliity;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Admin.Api.Controllers
{
    public class BaseController : ApiController
    {
        public FactoryUtiilty factory = new FactoryUtiilty();

        protected async Task<Deposit> GetDepositByIdAndPaymentMethod(int id, PaymentMethod paymentMethod)
        {
            switch (paymentMethod)
            {
                case PaymentMethod.Alipay:
                    return await factory.AlipayDepositService.Find(id, "Merchant");
                case PaymentMethod.WeChat:
                    return await factory.WeChatDepositService.Find(id, "Merchant");
                case PaymentMethod.OnlineBanking:
                    return await factory.OnlineBankingDepositService.Find(id, "Merchant");
                case PaymentMethod.PrepaidCard:
                    return await factory.PrepaidCardDepositService.Find(id, "Merchant");
                case PaymentMethod.QQWallet:
                    return await factory.QQWalletDepositService.Find(id, "Merchant");
                case PaymentMethod.QuickPay:
                    return await factory.QuickPayDepositService.Find(id, "Merchant");
                case PaymentMethod.OnlineToBankCard:
                    return await factory.OnlineToBankCardDepositService.Find(id, "Merchant");
                case PaymentMethod.JDWallet:
                    return await factory.JDWalletDepositService.Find(id, "Merchant");
                case PaymentMethod.Internal:
                    return await factory.InternalDepositService.Find(id, "Merchant");

                default:
                    throw new BusinessException("不支援的 PaymentMethod");
            }
        }

        protected TransactionStatus TransferTransactionStatus(DepositStatus status)
        {
            switch (status)
            {
                case DepositStatus.Unknown:
                    return TransactionStatus.UnKnow;
                case DepositStatus.PendingPayment:
                case DepositStatus.AwaitReminderApproval:
                    return TransactionStatus.PendingPayment;
                case DepositStatus.AutoSuccess:
                case DepositStatus.ManualSuccess:
                case DepositStatus.Success:
                    return TransactionStatus.Success;
                case DepositStatus.PaymentFailed:
                    return TransactionStatus.Fail;
                default:
                    throw new BusinessException("此订单无订单状态");
            }
        }

        protected TransactionStatus TransferTransactionStatus(WithdrawalStatus status)
        {
            switch (status)
            {
                case WithdrawalStatus.AutoPaymentSuccessful:
                case WithdrawalStatus.ManualConfirmed:
                case WithdrawalStatus.RetryAutoPaymentSuccessful:
                case WithdrawalStatus.RetryManualConfirmed:
                    return TransactionStatus.Success;
                case WithdrawalStatus.AutoPaymentFailed:
                case WithdrawalStatus.AutoPaymentManualConfirmedFailed:
                case WithdrawalStatus.FailedAndClosed:
                    return TransactionStatus.Fail;
                case WithdrawalStatus.AwaitingApproval:
                case WithdrawalStatus.AwaitingAutoPayment:
                case WithdrawalStatus.AutoPaymentInProgress:
                    return TransactionStatus.PendingPayment;
                default:
                    throw new BusinessException("此订单无订单状态");
            }
        }
    }
}
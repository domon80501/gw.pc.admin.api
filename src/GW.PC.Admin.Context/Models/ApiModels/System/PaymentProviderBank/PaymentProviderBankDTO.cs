﻿using Newtonsoft.Json;
using GW.PC.Models;
using GW.PC.Core;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentProviderBank
{
    public class PaymentProviderBankDTO
    {
		[JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("BankId")]
        public int BankId { get; set; }
        [JsonProperty("paymentProviderId")]
        public int PaymentProviderId { get; set; }
        [JsonProperty("paymentTypeId")]
        public int PaymentType { get; set; }
        [JsonProperty("paymentMethodId")]
        public int PaymentMethod { get; set; }
        [JsonProperty("merchantId")]
        public int MerchantId { get; set; }
        [JsonProperty("paymentProviderBankCode")]
        public string PaymentProviderBankCode { get; set; }
        [JsonProperty("order")]
        public int Order { get; set; }
        [JsonProperty("status")]
        public EntityStatus Status { get; set; }
    }
}

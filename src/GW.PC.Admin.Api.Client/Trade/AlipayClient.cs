﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.Alipay;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class AlipayClient : AdminApiClients
    {
        public AlipayClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "alipay";
        }

        public async Task<ApiResult<List<AlipayData>>> Search(AlipayQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<AlipayData>>(uri);
        }

        public async Task<ApiResult<AlipayDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<AlipayDTO>(uri);
        }
    }
}

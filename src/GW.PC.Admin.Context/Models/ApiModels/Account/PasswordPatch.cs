﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Account
{
    public class PasswordPatch
    {
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("oldPassword")]
        public string OldPassword { get; set; }
        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }
    }
}

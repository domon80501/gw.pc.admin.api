﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Merchants.Merchant;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Merchant
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiMerchant)]
    public class MerchantController : BaseController
    {
        [Route("merchant")]
        public void Options()
        { }

        [Route("merchant")]
        [CustomAuthorization(Permission = PermissionCode.Merchant_Access)]
        public async Task<IEnumerable<MerchantData>> Get([FromUri]MerchantQuery req)
        {
            var merchants = await factory.MerchantService.QueryAsync(req.Content);

            var result = merchants.Select(o => new MerchantData()
            {
                Id = o.Id,
                UserName = o.Username,
                Name = o.Name,
                MerchantKey = o.MerchantKey,
                Notes=o.Notes,
                Status=(int)o.Status,
                CreatedAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CreatedByUserName = o.CreatedBy?.Username,
            });

            return await Task.FromResult(result);
        }

        [Route("merchant")]
        public async Task<MerchantDTO> Get(int id)
        {
            var merchant = await factory.MerchantService.Find(id);

            var result = new MerchantDTO()
            {
                Id = merchant.Id,
                UserName = merchant.Username,
                Name = merchant.Name,
                MerchantKey = merchant.MerchantKey,
                Notes = merchant.Notes,
                Status = (int)merchant.Status,
                CreatedAt = merchant.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CreatedByUserName = merchant.CreatedBy?.Username,
            };

            return await Task.FromResult(result);
        }

        [Route("merchant")]
        [CustomAuthorization(Permission = PermissionCode.Merchant_Create)]
        public async Task Post(MerchantDTO req)
        {
            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            Models.Merchant merchant = new Models.Merchant()
            {
                Username = req.UserName,
                Name = req.Name,
                MerchantKey = req.MerchantKey,
                Notes = req.Notes,
                Status = (EntityStatus)req.Status,
                CreatedAt = DateTime.Now.ToE8(),
                CreatedById = createId,
            };

            await factory.MerchantService.Save(merchant);
        }

        [Route("merchant")]
        [CustomAuthorization(Permission = PermissionCode.Merchant_Detail_Edit)]
        public async Task Put(MerchantDTO req)
        {
            Models.Merchant merchant = await factory.MerchantService.GetByUsername(req.UserName);

            merchant.Name = req.Name;
            merchant.MerchantKey = req.MerchantKey;
            merchant.Notes = req.Notes;

            await factory.MerchantService.Save(merchant);
        }

        [Route("merchant")]
        [CustomAuthorization(Permission = PermissionCode.Merchant_Detail_Status_Edit)]
        public async Task Patch(MerchantStatus req)
        {
            int createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? 1 : int.Parse(HttpContext.Current.User.Identity.Name);
            await factory.MerchantService.UpdateEntityStatus(req.Id, (EntityStatus)req.Status, createId);
        }
    }
}
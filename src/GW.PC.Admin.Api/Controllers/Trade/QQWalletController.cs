﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels;
using GW.PC.Admin.Context.Models.ApiModels.Trade.QQWallet;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiDeposit)]
    public class QQWalletController : BaseController
    {
        [Route("qqwallet")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        public async Task<IEnumerable<QQWalletData>> Get([FromUri]QQWalletQuery req)
        {
            var qqWalletDeposits = await factory.QQWalletDepositService.QueryAsync(req.Content, "Merchant", "PaymentChannel");

            var result = qqWalletDeposits.Select(o => new QQWalletData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(o.Status),
                Feature = new Context.Models.Feature(o.Status)
            });

            return await Task.FromResult(result);
        }

        //[Route("qqwallet")]
        //[CustomAuthorization(Permission = PermissionCode.Order_Deposit_Access)]
        //public async Task<DataSourceResult<QQWalletData>> Get([FromUri]QQWalletQuery req)
        //{
        //    var qqWalletDeposits = await factory.QQWalletDepositService.QueryAsync(req.Content, int.Parse(req.PageNumber), int.Parse(req.PageSize), req.ColName, req.Descending, "Merchant", "PaymentChannel");
        //    var totalCount = await factory.QQWalletDepositService.QueryCountAsync(req.Content);
        //    var data = qqWalletDeposits.Select(o => new QQWalletData()
        //    {
        //        Id = o.Id,
        //        MerchantName = o.Merchant.Name,
        //        CustomerUserName = o.Username,
        //        Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
        //        MerchantOrderNumber = o.MerchantOrderNumber,
        //        PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
        //        CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
        //        CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
        //        PaymentChannelName = o.PaymentChannel.Name,
        //        DepositStatus = (int)o.Status,
        //    });

        //    var result = new DataSourceResult<QQWalletData>
        //    {
        //        Count = totalCount,
        //        Data = data
        //    };

        //    return await Task.FromResult(result);
        //}

        [Route("qqwallet")]
        public async Task<QQWalletDTO> Get(int id)
        {
            var qqWallet = await factory.QQWalletDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new QQWalletDTO()
            {
                Id = qqWallet.Id,
                MerchantName = qqWallet.Merchant.Name,
                CustomerUserName = qqWallet.Username,
                DepositIP = qqWallet.DepositIP,
                Amount = qqWallet.ActualAmount.HasValue ? qqWallet.ActualAmount : qqWallet.RequestedAmount,
                PaymentChannelName = qqWallet.PaymentChannel.Name,
                DepositStatus = Core.Extensions.GetDisplayName(qqWallet.Status),
                Commission = qqWallet.Commission,
                DepositAddress = qqWallet.DepositAddress,
                CreateAt = qqWallet.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = qqWallet.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = qqWallet.MerchantOrderNumber,
                SystemOrderNumber = qqWallet.SystemOrderNumber,
                PaymentCenterOrderNumber = qqWallet.PaymentCenterOrderNumber,
                PaymentOrderNumber = qqWallet.PaymentOrderNumber,
                ManualApprovedAt = qqWallet.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = qqWallet.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }
    }
}
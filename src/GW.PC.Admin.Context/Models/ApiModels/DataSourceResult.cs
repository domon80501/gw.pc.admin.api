﻿using System.Collections.Generic;

namespace GW.PC.Admin.Context.Models.ApiModels
{
    public class DataSourceResult<T>
    {
        public int Count { get; set; }
        public IEnumerable<T> Data { get; set; }
    }
}

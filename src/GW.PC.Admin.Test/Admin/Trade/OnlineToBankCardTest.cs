﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineToBankCard;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.Trade
{
    [TestClass]
    public class OnlineToBankCardTest
    {
        private OnlineToBankCardClient _client;
        public OnlineToBankCardClient OnlineToBankCardClient
        {
            get
            {
                if (_client == null)
                    _client = new OnlineToBankCardClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            OnlineToBankCardQuery req = new OnlineToBankCardQuery();

            //req.MerchantId = 1;
            //req.CustomerUserName = "";
            //req.PaymentCenterOrderNumber = "";
            //req.MerchantOrderNumber = "";
            //req.DepositStatus = Models.DepositStatus.AutoSuccess;
            //req.PaymentChannelId = 1;
            //req.CreateAtStart = DateTime.Now;
            //req.CreateAtEnd = DateTime.Now;

            var result = await OnlineToBankCardClient.Search(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var onlineToBankCard = await new OnlineToBankCardDepositService().Add(new OnlineToBankCardDeposit
            {
                Username = "onlineToBankCardTest" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                MerchantOrderNumber = "CK1812141118571616",
                PaymentPlatform = PaymentPlatform.PC,
                RequestedAmount = 10,
                Status = DepositStatus.Unknown,
                Commission = 10,
                MerchantCallbackStatus = MerchantCallbackStatus.Await,
                CreatedAt = DateTime.Now,
                PaymentChannelId = 1,
                MerchantId = 1,
            });
            var result = await OnlineToBankCardClient.FindById(onlineToBankCard.Id);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

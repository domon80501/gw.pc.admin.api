﻿using GW.PC.Admin.Api.Client.SystemExtension;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GW.PC.Admin.Test.Admin.SystemExtension
{
    [TestClass]
    public class OptionsTest
    {
        private OptionsClient _client;
        public OptionsClient OptionsClient
        {
            get
            {
                if (_client == null)
                    _client = new OptionsClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                var name = "PaymentProvider";

                var result = await OptionsClient.Get(name);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}

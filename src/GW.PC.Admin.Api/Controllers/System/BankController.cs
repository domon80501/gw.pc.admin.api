﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.System.Bank;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.System
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystem)]
    public class BankController : BaseController
    {
        [Route("bank")]
        public void Options()
        { }

        [Route("bank")]
        [CustomAuthorization(Permission = PermissionCode.System_Bank_Access)]
        public async Task<IEnumerable<BankData>> Get([FromUri]BankQuery req)
        {

            var banks = await factory.BankService.QueryAsync(req.Content);
            return banks.Select(s => new BankData
            {
                Id = s.Id.ToString(),
                Name = s.Name,
                Code = s.Code,
                Url = s.Url,
                CreatedAt = s.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CreatedById = s.CreatedById.ToString()
            }).ToList();
        }

        [Route("bank")]
        public async Task<BankDTO> Get(int id)
        {
            var bank = await factory.BankService.Find(id);
            return new BankDTO()
            {
                Id = bank.Id,
                Name = bank.Name,
                Code = bank.Code,
                Url = bank.Url,
                CreatedAt = bank.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CreatedById = bank.CreatedById.ToString()
            };
        }

        [Route("bank")]
        [CustomAuthorization(Permission = PermissionCode.System_Bank_Create)]
        public async Task Post(BankDTO req)
        {
            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            Bank bank = new Bank
            {
                Name = req.Name,
                Code = req.Code,
                Url = req.Url,

                CreatedAt = DateTime.Now.ToE8(),
                CreatedById = createId

            };
            await factory.BankService.Save(bank);
        }

        [Route("bank")]
        [CustomAuthorization(Permission = PermissionCode.System_Bank_Detail_Edit)]
        public async Task Put(BankDTO req)
        {
            Bank bank = await factory.BankService.Find(req.Id);
            bank.Name = req.Name;
            bank.Code = req.Code;
            bank.Url = req.Url;
            await factory.BankService.Save(bank);
        }
    }
}

﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.JDWallet;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class JDWalletClient : AdminApiClients
    {
        public JDWalletClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "jdwallet";
        }

        public async Task<ApiResult<List<JDWalletData>>> Search(JDWalletQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<JDWalletData>>(uri);
        }

        public async Task<ApiResult<JDWalletDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<JDWalletDTO>(uri);
        }
    }
}

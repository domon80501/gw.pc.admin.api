﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.System.Role;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.System
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystem)]
    public class RoleController : BaseController
    {
        [Route("role")]
        public void Options()
        { }

        [Route("role")]
        [CustomAuthorization(Permission = PermissionCode.System_Role_Access)]
        public async Task<IEnumerable<RoleListData>> Get([FromUri]RoleQuery req)
        {
            var roles = await factory.RoleService.QueryAsync(req.Content, "Permissions", "CreatedBy");

            var result = roles.AsEnumerable().Select(o => new RoleListData()
            {
                Id = o.Id,
                RoleName = o.Name,
                Status = (int)o.Status,
                CreatedByUserName = o.CreatedBy?.Username,
                CreatedAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss")
            });

            return await Task.FromResult(result);
        }

        [Route("role")]
        public async Task<RoleDetail> Get(int id)
        {
            var role = await factory.RoleService.Find(id, "Permissions");

            var result = new RoleDetail()
            {
                Id = role.Id,
                RoleName = role.Name,
                Status = (int)role.Status,
                CreatedByUserName = role.CreatedBy?.Username,
                CreatedAt = role.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                Permissions = role.Permissions.Select(o => new SubPermission()
                {
                    Id = o.Id,
                    Name = o.Name,
                }).ToList()
            };

            return await Task.FromResult(result);
        }

        [Route("role")]
        [CustomAuthorization(Permission = PermissionCode.System_Role_Create)]
        public async Task Post(RoleCreate req)
        {
            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            Role role = new Role
            {
                Name = req.RoleName,
                CreatedAt = DateTime.Now.ToE8(),
                CreatedById = createId,
                Status = EntityStatus.Enabled,
            };

            await factory.RoleService.Save(role, req.PermissionIds);
        }

        [Route("role")]
        [CustomAuthorization(Permission = PermissionCode.System_Role_Detail_Edit)]
        public async Task Put(RoleUpdate req)
        {
            Role role = await factory.RoleService.Find(req.Id, "Permissions");
            role.Name = req.RoleName;
            await factory.RoleService.Save(role, req.PermissionIds);
        }

        [Route("role")]
        [CustomAuthorization(Permission = PermissionCode.System_Role_Detail_Status_Edit)]
        public async Task Patch(RoleStatus req)
        {
            int createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? 1 : int.Parse(HttpContext.Current.User.Identity.Name);
            await factory.RoleService.UpdateEntityStatus(req.Id, (EntityStatus)req.Status, createId);
        }
    }
}
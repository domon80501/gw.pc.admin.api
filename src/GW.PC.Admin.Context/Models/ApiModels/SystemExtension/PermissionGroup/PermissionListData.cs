﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GW.PC.Admin.Context.Models.ApiModels.SystemExtension.PermissionGroup
{
    public class PermissionListData
    {
        public PermissionListData()
        {
            SubPermissionGroups = new List<PermissionListData>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        public int? ParentId { get; set; }

        [JsonProperty("subPermissionGroups")]
        public List<PermissionListData> SubPermissionGroups { get; set; }

        [JsonProperty("permissions")]
        public List<SubPermission> Permissions { get; set; }
    }

    public class SubPermission
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("masterId")]
        public int MasterId { get; set; }
    }
}

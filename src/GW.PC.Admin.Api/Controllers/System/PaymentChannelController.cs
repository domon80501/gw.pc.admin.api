﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.System.PaymentChannel;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.System
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystem)]
    public class PaymentChannelController : BaseController
    {
        [Route("paymentChannel")]
        public void Options()
        { }

        [Route("paymentChannel")]
        [CustomAuthorization(Permission = PermissionCode.System_Paymentchannel_Access)]
        public async Task<IEnumerable<PaymentChannelData>> Get([FromUri]PaymentChannelQuery req)
        {
            var paymentChannels = await factory.PaymentChannelService.QueryAsync(req.Content, "Provider", "Merchant");
            
            var result = paymentChannels.Select(o => new PaymentChannelData()
            {
                Id = o.Id,
                Name = o.Name,
                MerchantName = o.Merchant.Name,
                ProviderName = o.Provider.Name,
                PaymentPlatformName = Extensions.GetDisplayName(o.PaymentPlatform),
                PaymentTypeName = Extensions.GetDisplayName(o.PaymentType),
                PaymentMethodName = Extensions.GetDisplayName(o.PaymentMethod),
                TotalLimit = o.TotalLimit.ToString("F2"),
                MinDepositAmount = o.MinAmount.ToString("F2"),
                MaxDepositAmount = o.MaxAmount.ToString("F2"),
                LastDepositAt = o.LastUsedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                Status = (int)o.Status,
            });

            return await Task.FromResult(result);
        }

        [Route("paymentChannel")]
        public async Task<PaymentChannelDTO> Get(int id)
        {
            var paymentChannel = await factory.PaymentChannelService.Find(id);

            var result = new PaymentChannelDTO()
            {
                Id = paymentChannel.Id,
                Name = paymentChannel.Name,
                MerchantId = paymentChannel.MerchantId,
                PaymentType = (int)paymentChannel.PaymentType,
                PaymentMethod = (int)paymentChannel.PaymentMethod,
                PaymentProviderId = paymentChannel.ProviderId,
                CommissionPercentage = paymentChannel.CommissionPercentage,
                MerchantUsername = paymentChannel.MerchantUsername,
                MerchantKey = paymentChannel.MerchantKey,
                Status = (int)paymentChannel.Status,
                PaymentPlatform = (int)paymentChannel.PaymentPlatform,
                CommissionConstants = paymentChannel.CommissionConstants,
                MaxAmount = paymentChannel.MaxAmount,
                MinAmount = paymentChannel.MinAmount,
                TotalLimit = paymentChannel.TotalLimit,
                MinimumWithdrawInterval = paymentChannel.MinimumWithdrawInterval,
                CommissionMinValue = paymentChannel.CommissionMinValue,
                RequestUrl = paymentChannel.RequestUrl,
                CallbackUrl = paymentChannel.CallbackUrl,
                QueryUrl = paymentChannel.QueryUrl,
                Arguments = paymentChannel.Arguments,
                AssemblyName = paymentChannel.AssemblyName,
                HandlerType = paymentChannel.HandlerType,
                Notes = paymentChannel.Notes,
                ExtensionRoute = (int)paymentChannel.ExtensionRoute,
                CallbackSuccessType = (int)paymentChannel.CallbackSuccessType,
            };

            return await Task.FromResult(result);
        }

        [Route("paymentChannel/successRate")]
        public async Task<SuccessRateDTO> Get([FromUri]PaymentChannelSuccessRateQuery req)
        {
            var paymentChannel = await factory.PaymentChannelService.Find(req.Id);

            SuccessRateDTO result = new SuccessRateDTO();
            result.Id = req.Id;

            switch (paymentChannel.PaymentMethod)
            {
                case PaymentMethod.Alipay:
                    result.SuccessRates = await factory.AlipayDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                case PaymentMethod.WeChat:
                    result.SuccessRates = await factory.WeChatDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                case PaymentMethod.OnlineBanking:
                    result.SuccessRates = await factory.OnlineBankingDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                case PaymentMethod.PrepaidCard:
                    result.SuccessRates = await factory.PrepaidCardDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                case PaymentMethod.QQWallet:
                    result.SuccessRates = await factory.QQWalletDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                case PaymentMethod.QuickPay:
                    result.SuccessRates = await factory.QuickPayDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                case PaymentMethod.OnlineToBankCard:
                    result.SuccessRates = await factory.OnlineToBankCardDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                case PaymentMethod.JDWallet:
                    result.SuccessRates = await factory.JDWalletDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                case PaymentMethod.Internal:
                    result.SuccessRates = await factory.InternalDepositService.CalculateSuccessRate(req.GetPeriodStartsAt(), req.GetPeriodEndsAt());
                    break;
                default:
                    result.SuccessRates = string.Empty;
                    break;
            }

            return result;
        }

        [Route("paymentChannel")]
        [CustomAuthorization(Permission = PermissionCode.System_Paymentchannel_Create)]
        public async Task Post(PaymentChannelDTO req)
        {
            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            PaymentChannel paymentChannel = new PaymentChannel()
            {
                Name = req.Name,
                MerchantId = req.MerchantId,
                PaymentType = (PaymentType)req.PaymentType,
                PaymentMethod = (PaymentMethod)req.PaymentMethod,
                ProviderId = req.PaymentProviderId,
                CommissionPercentage = req.CommissionPercentage,
                MerchantUsername = req.MerchantUsername,
                MerchantKey = req.MerchantKey,
                Status = (EntityStatus)req.Status,
                PaymentPlatform = (PaymentPlatform)req.PaymentPlatform,
                CommissionConstants = req.CommissionConstants,
                MaxAmount = req.MaxAmount,
                MinAmount = req.MinAmount,
                TotalLimit = req.TotalLimit,
                MinimumWithdrawInterval = req.MinimumWithdrawInterval,
                CommissionMinValue = req.CommissionMinValue,
                RequestUrl = req.RequestUrl,
                QueryUrl = req.QueryUrl,
                Arguments = req.Arguments,
                AssemblyName = req.AssemblyName,
                HandlerType = req.HandlerType,
                Notes = req.Notes,
                CreatedAt = DateTime.Now.ToE8(),
                CreatedById = createId,
                ExtensionRoute = (ExtensionRoute)req.ExtensionRoute,
                CallbackSuccessType = (CallbackSuccessType)req.CallbackSuccessType,
            };

            var pc = await factory.PaymentChannelService.Save(paymentChannel);
            paymentChannel.CallbackUrl = CreatePaymentCenterCallbackUrl(pc);

            await factory.PaymentChannelService.Update(paymentChannel);
        }

        [Route("paymentChannel")]
        [CustomAuthorization(Permission = PermissionCode.System_Paymentchannel_Detail_Edit)]
        public async Task Put(PaymentChannelDTO req)
        {
            PaymentChannel paymentChannel = await factory.PaymentChannelService.Find(req.Id);

            paymentChannel.Name = req.Name;
            paymentChannel.MerchantId = req.MerchantId;
            paymentChannel.PaymentType = (PaymentType)req.PaymentType;
            paymentChannel.PaymentMethod = (PaymentMethod)req.PaymentMethod;
            paymentChannel.ProviderId = req.PaymentProviderId;
            paymentChannel.CommissionPercentage = req.CommissionPercentage;
            paymentChannel.MerchantUsername = req.MerchantUsername;
            paymentChannel.MerchantKey = req.MerchantKey;
            paymentChannel.PaymentPlatform = (PaymentPlatform)req.PaymentPlatform;
            paymentChannel.CommissionConstants = req.CommissionConstants;
            paymentChannel.MaxAmount = req.MaxAmount;
            paymentChannel.MinAmount = req.MinAmount;
            paymentChannel.TotalLimit = req.TotalLimit;
            paymentChannel.MinimumWithdrawInterval = req.MinimumWithdrawInterval;
            paymentChannel.CommissionMinValue = req.CommissionMinValue;
            paymentChannel.RequestUrl = req.RequestUrl;
            paymentChannel.QueryUrl = req.QueryUrl;
            paymentChannel.Arguments = req.Arguments;
            paymentChannel.AssemblyName = req.AssemblyName;
            paymentChannel.HandlerType = req.HandlerType;
            paymentChannel.Notes = req.Notes;
            paymentChannel.ExtensionRoute = (ExtensionRoute)req.ExtensionRoute;
            paymentChannel.CallbackSuccessType = (CallbackSuccessType)req.CallbackSuccessType;

            paymentChannel.CallbackUrl = CreatePaymentCenterCallbackUrl(paymentChannel);

            await factory.PaymentChannelService.Save(paymentChannel);
        }

        [Route("paymentChannel")]
        [CustomAuthorization(Permission = PermissionCode.System_Paymentchannel_Detail_Status_Edit)]
        public async Task Patch(PaymentChannelStatus req)
        {
            int createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? 1 : int.Parse(HttpContext.Current.User.Identity.Name);
            await factory.PaymentChannelService.UpdateEntityStatus(req.Id, (EntityStatus)req.Status, createId);
        }

        private string CreatePaymentCenterCallbackUrl(PaymentChannel paymentChannel)
        {
            return
                $"{Constants.AppSettings.Get(AppSettingNames.PaymentCallbackGatewayWebApiAddress)}/" +                
                $"{(paymentChannel.PaymentType == PaymentType.Deposit ? WebApiControllerRoutePrefixes.PaymentCallbackGateway : WebApiControllerRoutePrefixes.WithdrawCallbackGateway)}/" +
                // {paymentchannelid}/{paymenttype}/{paymentmethod}/{extensionroute}/{successtype}
                $"{paymentChannel.Id}/" +
                $"{(int)paymentChannel.PaymentType}/" +
                $"{(int)paymentChannel.PaymentMethod}/" +
                $"{(int)paymentChannel.ExtensionRoute}/" +
                $"{(int)paymentChannel.CallbackSuccessType}".ToLower();
        }
    }
}
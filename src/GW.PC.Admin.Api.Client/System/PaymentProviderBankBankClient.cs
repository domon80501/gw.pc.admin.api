﻿using GW.PC.Admin.Context.Models.ApiModels.System.PaymentProviderBank;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.System
{
    public class PaymentProviderBankClient : AdminApiClients
    {
        public PaymentProviderBankClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystem;
            uri = "paymentProviderBank";
        }

        public async Task<ApiResult<List<PaymentProviderBankData>>> Search(PaymentProviderBankQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<PaymentProviderBankData>>(uri);
        }

        public async Task<ApiResult<PaymentProviderBankDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<PaymentProviderBankDTO>(uri);
        }

        public async Task<ApiResult> Create(PaymentProviderBankDTO req)
        {
            return await PostApiResultAsync(uri, req);
        }

        public async Task<ApiResult> Update(PaymentProviderBankDTO req)
        {
            return await PutApiResultAsync(uri, req);
        }

        public async Task<ApiResult> UpdateStatus(PaymentProviderBankStatus req)
        {
            return await PatchApiResultAsync(uri, req);
        }
    }
}

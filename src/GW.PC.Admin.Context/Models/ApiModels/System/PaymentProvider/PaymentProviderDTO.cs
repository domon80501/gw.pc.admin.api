﻿using GW.PC.Models;
using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.PaymentProvider
{
    public class PaymentProviderDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("merchantId")]
        public int MerchantId { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("createdById")]
        public string CreatedById { get; set; }
        [JsonProperty("status")]
        public EntityStatus Status { get; set; }
    }
}

﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.Internal;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class InternalClient : AdminApiClients
    {
        public InternalClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "internal";
        }

        public async Task<ApiResult<List<InternalData>>> Search(InternalQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<InternalData>>(uri);
        }

        public async Task<ApiResult<InternalDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<InternalDTO>(uri);
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.SystemExtension;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GW.PC.UnitTest.Admin.SystemExtensiion
{
    [TestClass]
    public class PermissionGroupTest
    {
        private PermissionGroupCliient _client;
        public PermissionGroupCliient PermissionGroupCliient
        {
            get
            {
                if (_client == null)
                    _client = new PermissionGroupCliient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                var result = await PermissionGroupCliient.Search();

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}

﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Core;
using GW.PC.Merchant.Gateway.Api.Client;
using GW.PC.Merchant.Gateway.Context;
using GW.PC.Models;
using GW.PC.Models.Infrastructure;
using GW.PC.Models.Queues;
using GW.PC.Payment;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade.Feature
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiTradeFeature)]
    public class QueryController : BaseController
    {
        [Route("autoquery")]
        public async Task Get(int id, PaymentMethod paymentMethod)
        {
            Deposit deposit = await GetDepositByIdAndPaymentMethod(id, paymentMethod);

            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            PaymentQueryRequest paymentQueue = new PaymentQueryRequest()
            {
                RequestId = Guid.NewGuid().ToString(),
                MerchantUsername = deposit.Merchant.Username,
                PaymentType = PaymentType.Deposit,
                PaymentMethod = paymentMethod,
                MerchantOrderNumber = deposit.MerchantOrderNumber,
                CreatedAt = DateTime.Now.ToE8(),
                NextSendingAt = DateTime.Now.ToE8(),
            };

            await factory.PaymentQueryRequestService.Add(paymentQueue);
        }

        [Route("autoquery")]
        public async Task Get(int id)
        {
            MerchantWithdrawal withdrawal = await factory.MerchantWithdrawalService.Find(id, "Merchant", "PaymentChannel");

            int? createId = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? (int?)null : int.Parse(HttpContext.Current.User.Identity.Name);
            PaymentQueryRequest paymentQueue = new PaymentQueryRequest()
            {
                RequestId = Guid.NewGuid().ToString(),
                MerchantUsername = withdrawal.Merchant.Username,
                PaymentType = PaymentType.Withdrawal,
                PaymentMethod = withdrawal.PaymentChannel.PaymentMethod,
                MerchantOrderNumber = withdrawal.MerchantOrderNumber,
                CreatedAt = DateTime.Now.ToE8(),
                NextSendingAt = DateTime.Now.ToE8(),
            };

            await factory.PaymentQueryRequestService.Add(paymentQueue);
        }

        [Route("query")]
        [CustomAuthorization(Permission = PermissionCode.Order_Deposit_Query)]
        public async Task<string> Post(int id, PaymentMethod paymentMethod)
        {
            Deposit deposit = await GetDepositByIdAndPaymentMethod(id, paymentMethod);

            var request = new MerchantQueryRequestContext
            {
                MerchantOrderNumber = deposit.MerchantOrderNumber,
                MerchantUsername = deposit.Merchant.Username,
                PaymentMethod = ((int)paymentMethod).ToString(),
            };
            request.Sign = HttpUtility.UrlEncode(VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(request, string.Empty, string.Empty), deposit.Merchant.MerchantKey));

            var response = await new MerchantDepositsGatewayClient().Query(request);
            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());
            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            return context.Message;
        }

        [Route("autoquery")]
        [CustomAuthorization(Permission = PermissionCode.Order_MerchantWithdrawal_Query)]
        public async Task<string> Post(int id)
        {
            MerchantWithdrawal withdrawal = await factory.MerchantWithdrawalService.Find(id, "Merchant");

            var request = new MerchantQueryRequestContext
            {
                MerchantOrderNumber = withdrawal.MerchantOrderNumber,
                MerchantUsername = withdrawal.Merchant.Username,
            };
            request.Sign = HttpUtility.UrlEncode(VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(request, string.Empty, string.Empty), withdrawal.Merchant.MerchantKey));

            var response = await new MerchantDepositsGatewayClient().Query(request);
            var httpMsg = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());
            var context = JsonConvert.DeserializeObject<MerchantResponseContext>(httpMsg);

            return context.Message;
        }
    }
}
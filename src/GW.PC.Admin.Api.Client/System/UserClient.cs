﻿using GW.PC.Admin.Context.Models.ApiModels.System.User;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.System
{
    public class UserClient : AdminApiClients
    {
        public UserClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystem;
            uri = "user";
        }

        public async Task<ApiResult<List<UserListData>>> Search(UserQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<UserListData>>(uri);
        }

        public async Task<ApiResult<UserDetail>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<UserDetail>(uri);
        }

        public async Task<ApiResult> Create(UserCreate req)
        {
            return await PostApiResultAsync(uri, req);
        }

        public async Task<ApiResult> Update(UserUpdate req)
        {
            return await PutApiResultAsync(uri, req);
        }

        public async Task<ApiResult> UpdateStatus(UserStatus req)
        {
            return await PatchApiResultAsync(uri, req);
        }
    }
}

﻿using GW.PC.Admin.Api.Client.Trade;
using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlinePayment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GW.PC.UnitTest.Admin.Trade
{
    [TestClass]
    public class OnlinePaymentTest
    {
        private OnlinePaymentClient _client;
        public OnlinePaymentClient OnlinePaymentClient
        {
            get
            {
                if (_client == null)
                    _client = new OnlinePaymentClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                OnlinePaymentQuery req = new OnlinePaymentQuery();

                //req.MerchantId = 1;
                //req.CustomerUserName = "";
                //req.PaymentCenterOrderNumber = "";
                //req.MerchantOrderNumber = "";
                //req.DepositStatus = Models.DepositStatus.AutoSuccess;
                //req.PaymentChannelId = 1;
                //req.CreateAtStart = DateTime.Now;
                //req.CreateAtEnd = DateTime.Now;

                var result = await OnlinePaymentClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            try
            {
                int id = 1;
                var result = await OnlinePaymentClient.FindById(id);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}

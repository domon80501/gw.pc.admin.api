﻿using System;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.System;
using GW.PC.Admin.Context.Enum;
using GW.PC.Admin.Context.Models.ApiModels.System.EntityLog;
using GW.PC.Core;
using GW.PC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using GW.PC.Services.EF;

namespace GW.PC.Admin.Test.Admin.System
{
    [TestClass]
    public class EntityLogTest
    {
        private EntityLogClient _client;
        public EntityLogClient EntityLogClient
        {
            get
            {
                if (_client == null)
                    _client = new EntityLogClient();

                return _client;
            }
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            try
            {
                EntityLogQuery req = new EntityLogQuery();

                var result = await EntityLogClient.Search(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            try
            {
                var etList = await new EntityLogService().GetAll();
                EntityLog et = etList.FirstOrDefault();

                while (et == null)
                {
                    InsertEntityLog();

                    etList = await new EntityLogService().GetAll();
                    et = etList.FirstOrDefault(o => o.Details.Contains("支付宝转卡"));
                }

                int id = et.Id;
                var result = await EntityLogClient.FindById(id);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private async void InsertEntityLog()
        {
            await EntityLogService.Add(
                EntityLogContent.Deposit_Created,
                EntityLogTargetType.AlipayDeposit,
                1,
                "支付宝转卡",
                "支付宝"
                );
        }
    }
}

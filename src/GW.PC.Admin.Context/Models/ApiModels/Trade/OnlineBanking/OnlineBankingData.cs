﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineBanking
{
    public class OnlineBankingData : TradeBase
    {
        [JsonProperty("customerUserName")]
        public string CustomerUserName { get; set; }
        [JsonProperty("requestedAmount")]
        public decimal? Amount { get; set; }
        [JsonProperty("bankName")]
        public string BankName { get; set; }
        [JsonProperty("payerName")]
        public string PayerName { get; set; }
        [JsonProperty("merchantOrderNumber")]
        public string MerchantOrderNumber { get; set; }
        [JsonProperty("paymentCenterOrderNumber")]
        public string PaymentCenterOrderNumber { get; set; }
        [JsonProperty("createAt")]
        public string CreateAt { get; set; }
    }
}

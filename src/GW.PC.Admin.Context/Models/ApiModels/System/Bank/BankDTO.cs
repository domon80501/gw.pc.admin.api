﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.Bank
{
    public class BankDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("createdById")]
        public string CreatedById { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade
{
    public abstract class TradeBase
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("merchantName")]
        public string MerchantName { get; set; }
        [JsonProperty("paymentChannelName")]
        public string PaymentChannelName { get; set; }
        [JsonProperty("depositStatus")]
        public string DepositStatus { get; set; }
        [JsonProperty("feature")]
        public Feature Feature { get; set; }
    }
}

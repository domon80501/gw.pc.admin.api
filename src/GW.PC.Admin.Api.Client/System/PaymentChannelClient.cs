﻿using GW.PC.Admin.Context.Models.ApiModels.System.PaymentChannel;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.System
{
    public class PaymentChannelClient : AdminApiClients
    {
        public PaymentChannelClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystem;
            uri = "paymentChannel";
        }

        public async Task<ApiResult<List<PaymentChannelData>>> Search(PaymentChannelQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<PaymentChannelData>>(uri);
        }

        public async Task<ApiResult<PaymentChannelDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<PaymentChannelDTO>(uri);
        }
        
        public async Task<ApiResult<SuccessRateDTO>> GetSuccessRate(PaymentChannelSuccessRateQuery req, string route)
        {
            uri = $"{uri}/{route}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<SuccessRateDTO>(uri);
        }

        public async Task<ApiResult> Create(PaymentChannelDTO req)
        {
            return await PostApiResultAsync(uri, req);
        }

        public async Task<ApiResult> Update(PaymentChannelDTO req)
        {
            return await PutApiResultAsync(uri, req);
        }

        public async Task<ApiResult> UpdateStatus(PaymentChannelStatus req)
        {
            return await PatchApiResultAsync(uri, req);
        }
    }
}

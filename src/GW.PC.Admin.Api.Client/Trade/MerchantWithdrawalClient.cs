﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.MerchantWithdrawal;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class MerchantWithdrawalClient : AdminApiClients
    {
        public MerchantWithdrawalClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiWithdraw;
            uri = "merchantWithdrawal";
        }

        public async Task<ApiResult<List<MerchantWithdrawalData>>> Search(MerchantWithdrawalQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<MerchantWithdrawalData>>(uri);
        }

        public async Task<ApiResult<MerchantWithdrawalDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<MerchantWithdrawalDTO>(uri);
        }
    }
}

﻿using GW.PC.Admin.Context.Models.ApiModels.Account;
using GW.PC.Web.Core;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Account
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiAccount)]
    public class PasswordController : BaseController
    {
        [Route("password")]
        public void Options()
        { }

        [Route("password")]
        public async Task Patch(PasswordPatch req)
        {
            await factory.UserService.ChangePassword(req.UserName, req.OldPassword, req.NewPassword);
        }
    }
}

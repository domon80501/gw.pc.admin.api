﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade.QQWallet
{
    public class QQWalletData : TradeBase
    {
        [JsonProperty("customerUserName")]
        public string CustomerUserName { get; set; }
        [JsonProperty("requestedAmount")]
        public decimal? Amount { get; set; }
        [JsonProperty("merchantOrderNumber")]
        public string MerchantOrderNumber { get; set; }
        [JsonProperty("paymentCenterOrderNumber")]
        public string PaymentCenterOrderNumber { get; set; }
        [JsonProperty("createAt")]
        public string CreateAt { get; set; }
        [JsonProperty("completedAt")]
        public string CompletedAt { get; set; }
    }
}

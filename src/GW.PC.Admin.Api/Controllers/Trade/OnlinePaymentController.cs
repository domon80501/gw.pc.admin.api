﻿using GW.PC.Admin.Api.Attribute;
using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlinePayment;
using GW.PC.Models;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.Trade
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiTrade)]
    public class OnlinePaymentController : BaseController
    {
        [Route("onlinepayment")]
        [CustomAuthorization(Permission = PermissionCode.Order_OnlinePaymentDeposit_Access)]
        public async Task<IEnumerable<OnlinePaymentData>> Get([FromUri]OnlinePaymentQuery req)
        {
            var onlinePaymentDeposits = await factory.OnlinePaymentDepositService.QueryAsync(req.Content, "PaymentChannel");

            var result = onlinePaymentDeposits.Select(o => new OnlinePaymentData()
            {
                Id = o.Id,
                MerchantName = o.Merchant.Name,
                CustomerUserName = o.Username,
                Amount = o.ActualAmount.HasValue ? o.ActualAmount : o.RequestedAmount,
                MerchantOrderNumber = o.MerchantOrderNumber,
                PaymentCenterOrderNumber = o.PaymentCenterOrderNumber,
                CreateAt = o.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = o.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                PaymentChannelName = o.PaymentChannel.Name,
                DepositStatus = (int)o.Status,
            });

            return await Task.FromResult(result);
        }

        [Route("onlinepayment")]
        public async Task<OnlinePaymentDTO> Get(int id)
        {
            var alipay = await factory.OnlinePaymentDepositService.Find(id, "Merchant", "PaymentChannel", "AuditedBy");

            var result = new OnlinePaymentDTO()
            {
                Id = alipay.Id,
                MerchantName = alipay.Merchant.Name,
                CustomerUserName = alipay.Username,
                DepositIP = alipay.DepositIP,
                Amount = alipay.ActualAmount.HasValue ? alipay.ActualAmount : alipay.RequestedAmount,
                PaymentChannelName = alipay.PaymentChannel.Name,
                DepositStatus = (int)alipay.Status,
                Commission = alipay.Commission,
                DepositAddress = alipay.DepositAddress,
                CreateAt = alipay.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss"),
                CompletedAt = alipay.CompletedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                MerchantOrderNumber = alipay.MerchantOrderNumber,
                SystemOrderNumber = alipay.SystemOrderNumber,
                PaymentCenterOrderNumber = alipay.PaymentCenterOrderNumber,
                PaymentOrderNumber = alipay.PaymentOrderNumber,
                ManualApprovedAt = alipay.AuditedAt?.ToString("yyyy/MM/dd HH:mm:ss"),
                ManualApprovedBy = alipay.AuditedBy?.Username,
            };

            return await Task.FromResult(result);
        }

        [Route("onlinepayment")]
        public async Task Patch(int id)
        {
            // todo 查寻还没做
        }
    }
}
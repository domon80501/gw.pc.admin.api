﻿using GW.PC.Core;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Handlers
{
    public class ApiResultMessageHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var guid = Guid.NewGuid().ToString();
            var content = await request.Content.ReadAsStringAsync();

            SerilogLogger.Logger.LogDebug($"Request received {request.Method.Method}- {guid}: Url: {request.RequestUri} Content: {content}");

            var response = await base.SendAsync(request, cancellationToken);

            if (response.IsSuccessStatusCode)
            {
                ApiResult result = new ApiResult();

                if(response.Content != null)
                {
                    var value = (response.Content as ObjectContent)?.Value;
                    // result = result.Success(value);
                    result.Result = value;
                }

                var responseContent = JsonConvert.SerializeObject(result, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                SerilogLogger.Logger.LogInformation($"Response sent - {guid}: {responseContent}");

                var responseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(responseContent,
                        Encoding.UTF8,
                        "application/json")
                };

                responseMessage.Headers.Add("Access-Control-Allow-Origin", "*");
                responseMessage.Headers.Add("Access-Control-Allow-Methods", "*");
                responseMessage.Headers.Add("Access-Control-Allow-Headers", "*");

                return responseMessage;
            }
            else
            {
                // exception filter is a better place for error handling.
                return response;
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.System;
using GW.PC.Admin.Context.Models.ApiModels.System.PaymentProviderBank;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GW.PC.Admin.Test.Admin.System
{
    [TestClass]
    public class PaymentProviderBankTest
    {
        private PaymentProviderBankClient _client;
        public PaymentProviderBankClient PaymentProviderBankClient
        {
            get
            {
                if (_client == null)
                    _client = new PaymentProviderBankClient();

                return _client;
            }
        }

        private async Task<bool> IsExistChannel(string name)
        {
            var providerList = await new PaymentProviderService().GetAll();
            var provider = providerList.FirstOrDefault(o => o.Name == name);

            return provider != null;
        }

        [TestMethod]
        public async Task TestPostMethod()
        {
            var name = "TestProviderBank_" + RandomGenerator.RandomString(10);

            bool isExist = await IsExistChannel(name);
            while (isExist)
            {
                name = "TestProviderBank_" + RandomGenerator.RandomString(10);
                isExist = await IsExistChannel(name);
            }

            var provider = await new PaymentProviderService().Add(new PaymentProvider
            {
                Name = name,
                CreatedAt = DateTime.Now,
                CreatedById = 1,
                MerchantId = 2,
                Status = EntityStatus.Enabled
            });

            PaymentProviderBankDTO req = new PaymentProviderBankDTO()
            {
                BankId = 1,
                Order = 1,
                PaymentMethod = 1,
                MerchantId = provider.MerchantId,
                PaymentProviderBankCode = "ICBC",
                PaymentProviderId = provider.Id,
                PaymentType = 1,
                Status = EntityStatus.Enabled
            };

            var result = await PaymentProviderBankClient.Create(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            PaymentProviderBankQuery req = new PaymentProviderBankQuery();

            req.Name = "";
            //req.ProviderId = 0;
            //req.PaymentPlatform = 0;
            //req.PaymentType = 0;
            //req.PaymentMethod = 0;
            //req.Status = 0;

            var result = await PaymentProviderBankClient.Search(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var ppbList = await new PaymentProviderBankService().GetAll();
            PaymentProviderBank providerBank = ppbList.FirstOrDefault(o => o.Code.Contains("ICBC"));

            while (providerBank == null)
            {
                await TestPostMethod();

                ppbList = await new PaymentProviderBankService().GetAll();
                providerBank = ppbList.FirstOrDefault(o => o.Code.Contains("ICBC"));
            }

            int id = providerBank.Id;
            var result = await PaymentProviderBankClient.FindById(id);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestPutMethod()
        {
            var ppbList = await new PaymentProviderBankService().GetAll();
            PaymentProviderBank providerBank = ppbList.FirstOrDefault(o => o.Code.Contains("ICBC"));

            while (providerBank == null)
            {
                await TestPostMethod();

                ppbList = await new PaymentProviderBankService().GetAll();
                providerBank = ppbList.FirstOrDefault(o => o.Code.Contains("ICBC"));
            }

            PaymentProviderBankDTO req = new PaymentProviderBankDTO()
            {
                Id = providerBank.Id,
                BankId = providerBank.BankId,
                Order = providerBank.Order,
                PaymentMethod = (int)providerBank.PaymentMethod,
                PaymentProviderBankCode = providerBank.Code,
                PaymentProviderId = providerBank.PaymentProviderId,
                PaymentType = (int)providerBank.PaymentType,
                Status = EntityStatus.Enabled
            };

            var result = await PaymentProviderBankClient.Update(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestPatchMethod()
        {
            PaymentProviderBankStatus req = new PaymentProviderBankStatus()
            {
                Id = 2,
                //Status = (int)EntityStatus.Disabled,
                //Status = (int)EntityStatus.Enabled,
                Status = EntityStatus.Deprecated,
            };

            var result = await PaymentProviderBankClient.UpdateStatus(req);

            Assert.IsTrue(result.Succeeded);
        }
    }
}

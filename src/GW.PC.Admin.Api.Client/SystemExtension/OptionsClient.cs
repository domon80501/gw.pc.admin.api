﻿using GW.PC.Admin.Context.Models.ApiModels.System.Options;
using GW.PC.Admin.Context.Models.ApiModels.SystemExtension.PermissionGroup;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.SystemExtension
{
    public class OptionsClient : AdminApiClients
    {
        public OptionsClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiSystemExtension;
            uri = "options";
        }

        public async Task<ApiResult<List<OptionsDTO>>> Get(string name)
        {
            uri = $"{uri}/{name}";
            return await GetApiResultAsync<List<OptionsDTO>>(uri);
        }
    }
}

﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.Trade.OnlinePayment
{
    public class OnlinePaymentDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("merchantName")]
        public string MerchantName { get; set; }
        [JsonProperty("customerUserName")]
        public string CustomerUserName { get; set; }
        [JsonProperty("depositIP")]
        public string DepositIP { get; set; }
        [JsonProperty("requestedAmount")]
        public decimal? Amount { get; set; }
        [JsonProperty("paymentChannelName")]
        public string PaymentChannelName { get; set; }
        [JsonProperty("depositStatus")]
        public int DepositStatus { get; set; }
        [JsonProperty("commission")]
        public decimal? Commission { get; set; }
        [JsonProperty("depositAddress")]
        public string DepositAddress { get; set; }
        [JsonProperty("createAt")]
        public string CreateAt { get; set; }
        [JsonProperty("completedAt")]
        public string CompletedAt { get; set; }
        [JsonProperty("merchantOrderNumber")]
        public string MerchantOrderNumber { get; set; }
        [JsonProperty("systemOrderNumber")]
        public string SystemOrderNumber { get; set; }
        [JsonProperty("paymentCenterOrderNumber")]
        public string PaymentCenterOrderNumber { get; set; }
        [JsonProperty("paymentOrderNumber")]
        public string PaymentOrderNumber { get; set; }
        [JsonProperty("manualApprovedAt")]
        public string ManualApprovedAt { get; set; }
        [JsonProperty("manualApprovedBy")]
        public string ManualApprovedBy { get; set; }
    }
}

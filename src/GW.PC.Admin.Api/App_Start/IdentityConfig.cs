﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF;
using GW.PC.Services.EF.Admin;
using GW.PC.Services.EF.Owin;
using GW.PC.Web.Core;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.IO;

namespace GW.PC.Admin.Api
{
    public class IdentityConfig
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            // 设定 OWIN DB and Service 
            app.CreatePerOwinContext(PCContext.Create);
            app.CreatePerOwinContext<IdentityUserService>(IdentityUserService.Create);
            //app.CreatePerOwinContext<RoleService>(RoleService.Create);


            // 使用 Cookie 验证
            app.UseCookieAuthentication(
                new CookieAuthenticationOptions
                {
                    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                    LoginPath = new PathString(Path.Combine(WebApiControllerRoutePrefixes.GWAdminApiAccount, "/login")),

                    // todo need test provider 
                    Provider = new CookieAuthenticationProvider
                    {
                        OnValidateIdentity = SecurityStampValidator
                        .OnValidateIdentity<IdentityUserService, AdminIdentityUser>
                        (
                             validateInterval: TimeSpan.FromMinutes(30),
                             regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager)
                           //  getUserIdCallback: (id) => id.GetUserId() == null ? 1 : (Int32.Parse(id.GetUserId()))
                        )
                    },
                });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ApplicationCookie);
            
        }
    }
}

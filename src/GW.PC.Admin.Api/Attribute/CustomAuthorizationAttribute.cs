﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace GW.PC.Admin.Api.Attribute
{
    public class CustomAuthorization : AuthorizeAttribute
    {
        public PermissionCode Permission { get; set; }
        
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (Authorize(actionContext))
            {
                return;
            }
            else
            {
                throw new BusinessException(Core.Constants.Messages.NoPermission);
                //HandleUnauthorizedRequest(actionContext);
            }
        }

        private bool Authorize(HttpActionContext actionContext)
        {
#if DEBUG
            //            return true;
            //#else

            try
            {
                var identity = HttpContext.Current.User.Identity as ClaimsIdentity;

                //var claimJson = JsonConvert.SerializeObject(identity.Claims);
                //SerilogLogger.Logger.LogInformation($"TimmyClaimsInAuth: identity: {claimJson}");


                if (identity != null)
                {

                    //var permissionJson = JsonConvert.SerializeObject(identity.FindAll("Permission"));
                    //SerilogLogger.Logger.LogInformation($"Timmy Permission: permissions: {permissionJson}");





                    var permissions = identity.FindAll("Permission");


                    return true;

                    // todo
                    //return permissions.Any(o => o.Value == Permission.ToString());
                }
                else
                {
                    return false;
                }
            }
            catch (System.Exception e)
            {
                SerilogLogger.Logger.LogInformation($"TimmyAuth: Exception: {e.Message}");
                // todo
                return true;
            }
#endif
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var result = new ApiResult
            {
                ResultCode = Constants.PCAdminMessages.PermissionErrorCode,
                IsBusinessError = false,
                ResultMessage = Constants.PCAdminMessages.PermissionNotAllowed
            };

            var responseContent = JsonConvert.SerializeObject(result, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            actionContext.Response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Forbidden,
                Content = new StringContent(responseContent,
                        Encoding.UTF8,
                        "application/json")
            };
        }
    }
}
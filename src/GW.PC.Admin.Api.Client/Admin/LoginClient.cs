﻿using GW.PC.Admin.Context.Models.ApiModels.Account;
using GW.PC.Admin.Context.Models.ApiModels.Merchants.Merchant;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Admin
{
    public class LoginClient : AdminApiClients
    {
        public LoginClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiAccount;
            uri = "login";
        }

        public async Task<ApiResult> DoLogin(LoginDTO req)
        {
            return await PostApiResultAsync(uri, req);
        }
    }
}

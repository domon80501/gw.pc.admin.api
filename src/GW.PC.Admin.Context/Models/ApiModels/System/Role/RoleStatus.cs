﻿using Newtonsoft.Json;

namespace GW.PC.Admin.Context.Models.ApiModels.System.Role
{
    public class RoleStatus
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
    }
}

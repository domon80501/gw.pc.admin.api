﻿using GW.PC.Admin.Context.Models.ApiModels.System.Options;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Models.Enums;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Admin.Api.Controllers.SystemExtension
{
    [RoutePrefix(WebApiControllerRoutePrefixes.GWAdminApiSystemExtension)]
    public class OptionsController : BaseController
    {
        [Route("options/{name}")]
        public async Task<IEnumerable<OptionsDTO>> GetTable(string name)
        {
            SystemOption item = (SystemOption)Enum.Parse(typeof(SystemOption), name, false);
            return await GetOptions(item);
        }

        [Route("options/{name}")]
        public async Task<IEnumerable<OptionsDTO>> GetTable(string name, int id)
        {
            SystemOption item = (SystemOption)Enum.Parse(typeof(SystemOption), name, false);
            return await GetOptions(item, id.ToString());
        }
        
        #region private method


        private async Task<IEnumerable<OptionsDTO>> GetOptions(SystemOption option, params string[] parms)
        {
            switch (option)
            {
                case SystemOption.PaymentType:
                    return GetEnumList(typeof(PaymentType));
                case SystemOption.PaymentMethod:
                    return GetEnumList(typeof(PaymentMethod));
                case SystemOption.EntityStatus:
                    return GetEnumList(typeof(EntityStatus));
                case SystemOption.PaymentPlatform:
                    return GetEnumList(typeof(PaymentPlatform));
                case SystemOption.DepositStatus:
                    return GetEnumList(typeof(DepositStatus));
                case SystemOption.EntityLogTargetType:
                    return GetEnumList(typeof(EntityLogTargetType));
                case SystemOption.ExtensionRoute:
                    return GetEnumList(typeof(ExtensionRoute));
                case SystemOption.CallbackSuccessType:
                    return GetEnumList(typeof(CallbackSuccessType));
                case SystemOption.WithdrawalStatus:
                    return GetEnumList(typeof(WithdrawalStatus));

                case SystemOption.PaymentProvider:
                    var paymentProviders = await factory.PaymentProviderService.QueryAsync(q => q.Status == EntityStatus.Enabled);
                    return paymentProviders.Select(s => new OptionsDTO
                    {
                        Id = s.Id,
                        Name = s.Name
                    }).ToList();
                case SystemOption.WithdrawalPaymentChannel:
                    int paymentType = Int32.Parse(parms[0]);
                    var withdrawPaymentChannels = await factory.PaymentChannelService.QueryAsync(q => q.Status == EntityStatus.Enabled
                        && q.PaymentType == (PaymentType)paymentType);
                    return withdrawPaymentChannels.Select(s => new OptionsDTO
                    {
                        Id = s.Id,
                        Name = s.Name
                    }).ToList();
                case SystemOption.PaymentChannel:
                    int paymentMethodpc = Int32.Parse(parms[0]);
                    var paymentChannels = await factory.PaymentChannelService.QueryAsync(q => q.Status == EntityStatus.Enabled
                        && q.PaymentMethod == (PaymentMethod)paymentMethodpc
                        && q.PaymentType == PaymentType.Deposit);
                    return paymentChannels.Select(s => new OptionsDTO
                    {
                        Id = s.Id,
                        Name = s.Name
                    }).ToList();
                case SystemOption.Role:
                    var roles = await factory.RoleService.QueryAsync(q => q.Status == EntityStatus.Enabled);
                    return roles.Select(s => new OptionsDTO
                    {
                        Id = s.Id,
                        Name = s.Name
                    }).ToList();
                case SystemOption.Merchant:
                    var merchants = await factory.MerchantService.QueryAsync(q => q.Status == EntityStatus.Enabled);
                    return merchants.Select(s => new OptionsDTO
                    {
                        Id = s.Id,
                        Name = s.Name
                    }).ToList();
                case SystemOption.Bank:
                    var banks = await factory.BankService.GetAll();
                    return banks.Select(s => new OptionsDTO
                    {
                        Id = s.Id,
                        Name = s.Name
                    }).ToList();
                default:
                    return GetEnumList(typeof(Enum));
            }
        }

        private IEnumerable<OptionsDTO> GetEnumList(Type option)
        {
            var result = Extensions.GenToEnumSource(option);

            // PayemntType 以后会陆续开启 (目前只开放 存款和提款
            //if(option == typeof(PaymentType))
            //{
            //    return result.Where(o => o.Name.ToString() == "存款" || o.Name.ToString() == "提款")
            //        .Select(s => new OptionsDTO
            //        {
            //            Id = int.Parse(s.Id.ToString()),
            //            Name = s.DisplayName == null ? s.Name.ToString() : s.DisplayName.ToString(),
            //        }).ToList();
            //}

            return result.Select(s => new OptionsDTO
            {
                Id = int.Parse(s.Id.ToString()),
                Name = s.DisplayName == null ? s.Name.ToString() : s.DisplayName.ToString(),
            }).ToList();
        }

        #endregion
    }
}
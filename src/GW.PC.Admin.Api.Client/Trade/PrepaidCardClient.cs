﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.PrepaidCard;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class PrepaidCardClient : AdminApiClients
    {
        public PrepaidCardClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "prepaidcard";
        }

        public async Task<ApiResult<List<PrepaidCardData>>> Search(PrepaidCardQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<PrepaidCardData>>(uri);
        }

        public async Task<ApiResult<PrepaidCardDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<PrepaidCardDTO>(uri);
        }
    }
}

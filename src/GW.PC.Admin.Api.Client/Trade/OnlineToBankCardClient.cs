﻿using GW.PC.Admin.Context.Models.ApiModels.Trade.OnlineToBankCard;
using GW.PC.Web.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GW.PC.Admin.Api.Client.Trade
{
    public class OnlineToBankCardClient : AdminApiClients
    {
        public OnlineToBankCardClient()
        {
            RoutePrefix = WebApiControllerRoutePrefixes.GWAdminApiDeposit;
            uri = "onlinetobankcard";
        }

        public async Task<ApiResult<List<OnlineToBankCardData>>> Search(OnlineToBankCardQuery req)
        {
            uri = $"{uri}?{Core.Extensions.SerializeToQueryString(req)}";
            return await GetApiResultAsync<List<OnlineToBankCardData>>(uri);
        }

        public async Task<ApiResult<OnlineToBankCardDTO>> FindById(int id)
        {
            uri = $"{uri}?id={id}";
            return await GetApiResultAsync<OnlineToBankCardDTO>(uri);
        }
    }
}

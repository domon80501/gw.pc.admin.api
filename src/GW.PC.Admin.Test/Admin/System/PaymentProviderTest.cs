﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GW.PC.Admin.Api.Client.System;
using GW.PC.Admin.Context.Models.ApiModels.System.PaymentProvider;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GW.PC.Admin.Test.Admin.System
{
    [TestClass]
    public class PaymentProviderTest
    {
        private PaymentProviderClient _client;
        public PaymentProviderClient PaymentProviderClient
        {
            get
            {
                if (_client == null)
                    _client = new PaymentProviderClient();

                return _client;
            }
        }

        private async Task<bool> IsExistChannel(string name)
        {
            var providerList = await new PaymentProviderService().GetAll();
            var provider = providerList.FirstOrDefault(o => o.Name == name);

            return provider != null;
        }

        [TestMethod]
        public async Task TestPostMethod()
        {
            try
            {
                var name = "TestProvider_" + RandomGenerator.RandomString(10);

                bool isExist = await IsExistChannel(name);
                while (isExist)
                {
                    name = "TestProvider_" + RandomGenerator.RandomString(10);
                    isExist = await IsExistChannel(name);
                }

                PaymentProviderDTO req = new PaymentProviderDTO()
                {
                    Name = name,
                    CreatedAt = DateTime.Now.ToString(),
                    CreatedById = "1",
                    MerchantId = 2,
                    Status = EntityStatus.Enabled
                };

                var result = await PaymentProviderClient.Create(req);

                Assert.IsTrue(result.Succeeded);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [TestMethod]
        public async Task TestGetMethod()
        {
            PaymentProviderQuery req = new PaymentProviderQuery();

            //req.Name = "";
            //req.Status = EntityStatus.Enabled;

            var result = await PaymentProviderClient.Search(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestGetByIdMethod()
        {
            var ppList = await new PaymentProviderService().GetAll();
            PaymentProvider provider = ppList.FirstOrDefault(o => o.Name.Contains("TestProvider_"));

            while (provider == null)
            {
                await TestPostMethod();

                ppList = await new PaymentProviderService().GetAll();
                provider = ppList.FirstOrDefault(o => o.Name.Contains("TestProvider_"));
            }

            int id = provider.Id;
            var result = await PaymentProviderClient.FindById(id);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestPutMethod()
        {
            var ppList = await new PaymentProviderService().GetAll();
            PaymentProvider provider = ppList.FirstOrDefault(o => o.Name.Contains("TestProvider_"));

            while (provider == null)
            {
                await TestPostMethod();

                ppList = await new PaymentProviderService().GetAll();
                provider = ppList.FirstOrDefault(o => o.Name.Contains("TestProvider_"));
            }

            PaymentProviderDTO req = new PaymentProviderDTO()
            {
                Id = provider.Id,
                Name = provider.Name,
                CreatedAt = DateTime.Now.ToString(),
                CreatedById = provider.CreatedById?.ToString(),
                Status = provider.Status
            };

            var result = await PaymentProviderClient.Update(req);

            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task TestPatchMethod()
        {
            var ppList = await new PaymentProviderService().GetAll();
            PaymentProvider provider = ppList.FirstOrDefault(o => o.Name.Contains("TestProvider_"));

            while (provider == null)
            {
                await TestPostMethod();

                ppList = await new PaymentProviderService().GetAll();
                provider = ppList.FirstOrDefault(o => o.Name.Contains("TestProvider_"));
            }

            PaymentProviderStatus req = new PaymentProviderStatus()
            {
                Id = provider.Id,
                //Status = (int)EntityStatus.Disabled,
                //Status = (int)EntityStatus.Enabled,
                Status = EntityStatus.Deprecated,
            };

            var result = await PaymentProviderClient.UpdateStatus(req);

            Assert.IsTrue(result.Succeeded);
        }
    }
}
